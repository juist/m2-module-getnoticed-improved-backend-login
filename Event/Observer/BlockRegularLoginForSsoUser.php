<?php

namespace GetNoticed\ImprovedBackendLogin\Event\Observer;

use Magento\Framework;
use Magento\Backend;
use Magento\User;
use GetNoticed\ImprovedBackendLogin as IBL;

class BlockRegularLoginForSsoUser
    implements Framework\Event\ObserverInterface
{

    // DI

    /**
     * @var IBL\Api\SsoUserRepositoryInterface
     */
    protected $ssoUserRepository;

    /**
     * @var Backend\Model\Auth\Session
     */
    protected $backendAuthSession;

    /**
     * @var Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var Framework\App\ResponseInterface|\Magento\Framework\App\Response\Http
     */
    protected $response;

    /**
     * @var Backend\Model\UrlInterface
     */
    protected $backendUrl;

    public function __construct(
        IBL\Api\SsoUserRepositoryInterface $ssoUserRepository,
        Backend\Model\Auth\Session $backendAuthSession,
        Framework\Message\ManagerInterface $messageManager,
        Framework\App\ResponseInterface $response,
        Backend\Model\UrlInterface $backendUrl
    ) {
        $this->ssoUserRepository = $ssoUserRepository;
        $this->backendAuthSession = $backendAuthSession;
        $this->messageManager = $messageManager;
        $this->response = $response;
        $this->backendUrl = $backendUrl;
    }

    public function execute(Framework\Event\Observer $observer)
    {
        try {
            $adminUser = $this->getAdminUserFromObserver($observer);
        } catch (\TypeError $e) {
            // Unable to get admin user, for some reason? Do not continue.
            return;
        }

        try {
            $ssoUser = $this->getSsoUserFromObserver($observer);

            // SSO user was supplied, do nothing
            return;
        } catch (\TypeError $e) {
            // SSO user not supplied, continue
        }

        // See if we can find an SSO user for this admin user, if so, deny login.
        try {
            $ssoUser = $this->ssoUserRepository->getByAdminUser($adminUser);

            // SSO user found, illegal login!
            $this->backendAuthSession->destroy();

            $this->messageManager->addErrorMessage(
                __(
                    'Your session has been destroyed, because you are not authorized to log in with this account outside of SSO.'
                )
            );

            $this->response->setRedirect($this->backendUrl->getUrl('adminhtml/dashboard/index'))->sendResponse();
        } catch (Framework\Exception\NoSuchEntityException $e) {
            // No SSO user found, do nothing
            return;
        }
    }

    /**
     * @return \Magento\User\Api\Data\UserInterface|\Magento\User\Model\User
     */
    private function getAdminUserFromObserver(Framework\Event\Observer $observer): User\Api\Data\UserInterface
    {
        return $observer->getData('user');
    }

    /**
     * @return IBL\Api\Data\SsoUserInterface|IBL\Model\SsoUser
     */
    private function getSsoUserFromObserver(Framework\Event\Observer $observer): IBL\Api\Data\SsoUserInterface
    {
        return $observer->getData('sso_user');
    }

}