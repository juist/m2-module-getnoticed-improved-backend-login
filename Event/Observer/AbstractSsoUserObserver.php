<?php

namespace GetNoticed\ImprovedBackendLogin\Event\Observer;

use Magento\Framework;
use Magento\User;
use GetNoticed\ImprovedBackendLogin as IBL;

abstract class AbstractSsoUserObserver
    implements Framework\Event\ObserverInterface
{

    /**
     * @var User\Model\UserFactory
     */
    protected $userFactory;

    /**
     * @var User\Model\ResourceModel\User
     */
    protected $userResource;

    public function __construct(
        User\Model\UserFactory $userFactory,
        User\Model\ResourceModel\User $userResource
    ) {
        $this->userFactory = $userFactory;
        $this->userResource = $userResource;
    }

    /**
     * @return IBL\Api\Data\SsoUserInterface|IBL\Model\SsoUser
     */
    protected function getSsoUserObject(Framework\Event\Observer $observer): IBL\Api\Data\SsoUserInterface
    {
        return $observer->getData('object');
    }

    /**
     * @param IBL\Api\Data\SsoUserInterface|IBL\Model\SsoUser $ssoUser
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function getAdminUserBySsoUser(
        IBL\Api\Data\SsoUserInterface $ssoUser
    ): User\Model\User {
        $adminUser = $this->userFactory->create();
        $this->userResource->load($adminUser, $ssoUser->getUserId());

        if ($adminUser->getId() === null) {
            throw new Framework\Exception\NoSuchEntityException(
                __('No related admin user found for SSO user "%1"', $ssoUser->getId())
            );
        }

        return $adminUser;
    }

}