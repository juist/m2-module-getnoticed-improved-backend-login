<?php

namespace GetNoticed\ImprovedBackendLogin\Event\Observer;

use Magento\Framework;
use Magento\User;
use Magento\Authorization;
use GetNoticed\ImprovedBackendLogin as IBL;

abstract class AbstractSsoPermissionObserver
    implements Framework\Event\ObserverInterface
{

    /**
     * @var IBL\Providers\ProviderListInterface
     */
    protected $providerList;

    /**
     * @var Authorization\Model\RoleFactory
     */
    protected $roleFactory;

    /**
     * @var Authorization\Model\ResourceModel\Role
     */
    protected $roleResource;

    /**
     * @var IBL\Helper\WhitelistDomainsHelper
     */
    protected $whitelistDomainsHelper;

    /**
     * @var Framework\Serialize\Serializer\Json
     */
    protected $jsonSerializer;

    public function __construct(
        IBL\Providers\ProviderListInterface $providerList,
        Authorization\Model\RoleFactory $roleFactory,
        Authorization\Model\ResourceModel\Role $roleResource,
        IBL\Helper\WhitelistDomainsHelper $whitelistDomainsHelper,
        Framework\Serialize\Serializer\Json $jsonSerializer
    ) {
        $this->providerList = $providerList;
        $this->roleFactory = $roleFactory;
        $this->roleResource = $roleResource;
        $this->whitelistDomainsHelper = $whitelistDomainsHelper;
        $this->jsonSerializer = $jsonSerializer;
    }

    /**
     * @return IBL\Api\Data\SsoPermissionInterface|IBL\Model\SsoPermission
     */
    protected function getSsoPermissionObject(Framework\Event\Observer $observer): IBL\Api\Data\SsoPermissionInterface
    {
        return $observer->getData('object');
    }

    /**
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function getAdminRoleById(int $roleId): Authorization\Model\Role
    {
        $role = $this->roleFactory->create();
        $this->roleResource->load($role, $roleId);

        if ($role->getId() === null) {
            throw new Framework\Exception\NoSuchEntityException(
                __('No admin role with ID "%1" found', $roleId)
            );
        }

        return $role;
    }

}