<?php

namespace GetNoticed\ImprovedBackendLogin\Event\Observer;

use Magento\Framework;
use GetNoticed\ImprovedBackendLogin as IBL;

class SsoPermissionCollectionLoadExternalObjectsData
    implements Framework\Event\ObserverInterface
{

    // DI

    /**
     * @var Framework\Event\ManagerInterface
     */
    protected $eventManager;

    public function __construct(
        Framework\Event\ManagerInterface $eventManager
    ) {
        $this->eventManager = $eventManager;
    }


    public function execute(Framework\Event\Observer $observer)
    {
        try {
            $collection = $this->getCollectionFromObserver($observer);
        } catch (\TypeError $e) {
            return;
        }

        foreach ($collection->getItems() as $ssoPermission) {
            $this->eventManager->dispatch(
                'getnoticed_improvedbackendlogin_ssopermission_load_after',
                [
                    'object' => $ssoPermission
                ]
            );
        }
    }

    private function getCollectionFromObserver(
        Framework\Event\Observer $observer
    ): IBL\Model\ResourceModel\SsoPermission\Collection {
        return $observer->getData('getnoticed_improvedbackendlogin_ssopermission');
    }

}