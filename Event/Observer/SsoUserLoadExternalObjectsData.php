<?php

namespace GetNoticed\ImprovedBackendLogin\Event\Observer;

use Magento\Framework;
use GetNoticed\ImprovedBackendLogin as IBL;

class SsoUserLoadExternalObjectsData
    extends IBL\Event\Observer\AbstractSsoUserObserver
{

    public function execute(Framework\Event\Observer $observer)
    {
        try {
            $ssoUser = $this->getSsoUserObject($observer);
        } catch (\TypeError $e) {
            return;
        }

        try {
            $ssoUser->setData(IBL\Api\Data\SsoUserInterface::KEY_USER_ID, $ssoUser->getUser()->getId());
        } catch (\TypeError $e) {
            return;
        }
    }

}