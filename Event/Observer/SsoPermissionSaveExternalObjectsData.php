<?php

namespace GetNoticed\ImprovedBackendLogin\Event\Observer;

use Magento\Framework;
use GetNoticed\ImprovedBackendLogin as IBL;

class SsoPermissionSaveExternalObjectsData
    extends AbstractSsoPermissionObserver
{

    public function execute(Framework\Event\Observer $observer)
    {
        try {
            $ssoPermission = $this->getSsoPermissionObject($observer);
        } catch (Framework\Exception\NoSuchEntityException $e) {
            return;
        } catch (\TypeError $e) {
            return;
        }

        try {
            $ssoPermission->setData(
                IBL\Api\Data\SsoPermissionInterface::KEY_PROVIDER_CODE,
                $ssoPermission->getProvider()->getCode()
            );
        } catch (\Exception | \Error $e) {
            // Fail silently
        }

        try {
            $ssoPermission->setData(
                IBL\Api\Data\SsoPermissionInterface::KEY_ADMIN_ROLE,
                $ssoPermission->getAdminRole()->getId()
            );
        } catch (\Exception | \Error $e) {
            // Fail silently
        }

        try {
            $ssoPermission->setData(
                IBL\Api\Data\SsoPermissionInterface::KEY_WHITELIST_DOMAINS,
                $this->jsonSerializer->serialize(
                    array_map(
                        function (IBL\Api\Data\VO\WhitelistDomainObjectInterface $whitelistDomain) {
                            return $whitelistDomain->toArray();
                        },
                        $ssoPermission->getWhitelistDomains()
                    )
                )
            );
        } catch (\Exception | \Error $e) {
            // Fail silently
        }
    }

}