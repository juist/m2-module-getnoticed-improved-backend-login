<?php

namespace GetNoticed\ImprovedBackendLogin\Event\Observer;

use Magento\Framework;
use GetNoticed\ImprovedBackendLogin as IBL;

class SsoUserSaveExternalObjectsData
    extends AbstractSsoUserObserver
{

    public function execute(Framework\Event\Observer $observer)
    {
        try {
            $ssoUser = $this->getSsoUserObject($observer);
            $adminUser = $this->getAdminUserBySsoUser($ssoUser);
        } catch (Framework\Exception\NoSuchEntityException $e) {
            return;
        } catch (\TypeError $e) {
            return;
        }

        $ssoUser->setUser($adminUser);
    }

}