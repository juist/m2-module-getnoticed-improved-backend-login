<?php

namespace GetNoticed\ImprovedBackendLogin\Event\Observer;

use Magento\Authorization;
use Magento\Framework;
use GetNoticed\ImprovedBackendLogin as IBL;

class SsoPermissionLoadExternalObjectsData
    extends IBL\Event\Observer\AbstractSsoPermissionObserver
{

    public function execute(Framework\Event\Observer $observer)
    {
        try {
            $ssoPermission = $this->getSsoPermissionObject($observer);
        } catch (\TypeError $e) {
            return;
        }

        try {
            $ssoPermission->setProvider($this->providerList->getProviderByCode($ssoPermission->getProviderCode()));
        } catch (Framework\Exception\NoSuchEntityException | \TypeError $e) {
            // Failed to get provider
        }

        try {
            $ssoPermission->setAdminRole($this->getAdminRoleById($ssoPermission->getAdminRoleId()));
        } catch (Framework\Exception\NoSuchEntityException | \TypeError $e) {
            // Failed to get admin role
        }

        try {
            $ssoPermission->setWhitelistDomains(
                ...$this->whitelistDomainsHelper
                       ->convertJsonToWhitelistDomainsObjects($ssoPermission->getWhitelistDomainsJson())
            );
        } catch (\TypeError $e) {
            // Failed to load whitelist domains
        }
    }

}