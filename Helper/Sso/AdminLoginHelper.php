<?php

namespace GetNoticed\ImprovedBackendLogin\Helper\Sso;

use Magento\Framework;
use Magento\Backend;
use Magento\User;
use Magento\Security;

class AdminLoginHelper
    extends Framework\App\Helper\AbstractHelper
{

    // DI

    /**
     * @var Backend\Model\Auth
     */
    protected $auth;

    /**
     * @var Backend\Model\Auth\Session
     */
    protected $authSession;

    /**
     * @var Framework\Stdlib\CookieManagerInterface
     */
    protected $cookieManager;

    /**
     * @var Security\Model\Plugin\Auth
     */
    protected $securityAuthPlugin;

    /**
     * @var Backend\Model\UrlInterface
     */
    protected $backendUrl;

    public function __construct(
        Framework\App\Helper\Context $context,
        Backend\Model\Auth $auth,
        Backend\Model\Auth\Session $authSession,
        Framework\Stdlib\CookieManagerInterface $cookieManager,
        Security\Model\Plugin\Auth $securityAuthPlugin,
        Backend\Model\UrlInterface $backendUrl
    ) {
        $this->auth = $auth;
        $this->authSession = $authSession;
        $this->cookieManager = $cookieManager;
        $this->securityAuthPlugin = $securityAuthPlugin;
        $this->backendUrl = $backendUrl;

        parent::__construct($context);
    }

    // DI

    public function getAuth(): Backend\Model\Auth
    {
        return $this->auth;
    }

    public function getAuthSession(): Backend\Model\Auth\Session
    {
        return $this->authSession;
    }

    public function getCookieManager(): Framework\Stdlib\CookieManagerInterface
    {
        return $this->cookieManager;
    }

    public function getSecurityAuthPlugin(): Security\Model\Plugin\Auth
    {
        return $this->securityAuthPlugin;
    }

    public function getBackendUrl(): Backend\Model\UrlInterface
    {
        return $this->backendUrl;
    }

}