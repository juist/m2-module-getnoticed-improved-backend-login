<?php

namespace GetNoticed\ImprovedBackendLogin\Helper\Sso;

use Magento\Framework;
use Magento\Backend;
use Magento\User;
use GetNoticed\ImprovedBackendLogin as IBL;

class ExternalAdminUserHelper
    extends Framework\App\Helper\AbstractHelper
{

    // Variables

    /**
     * @var User\Model\User
     */
    protected $user;

    // DI

    /**
     * @var User\Model\UserFactory
     */
    protected $userFactory;

    /**
     * @var User\Model\ResourceModel\User
     */
    protected $userResource;

    /**
     * @var IBL\Helper\Sso\AdminLoginHelper
     */
    protected $adminLoginHelper;

    /**
     * @var Framework\Controller\ResultFactory
     */
    protected $resultFactory;

    /**
     * @var IBL\Api\SsoUserRepositoryInterface
     */
    protected $ssoUserRepository;

    /**
     * @var IBL\Model\SsoUserFactory
     */
    protected $ssoUserFactory;

    /**
     * @var IBL\Api\SsoPermissionRepositoryInterface
     */
    protected $ssoPermissionRepository;

    public function __construct(
        Framework\App\Helper\Context $context,
        User\Model\UserFactory $userFactory,
        User\Model\ResourceModel\User $userResource,
        IBL\Helper\Sso\AdminLoginHelper $adminLoginHelper,
        Framework\Controller\ResultFactory $resultFactory,
        IBL\Api\SsoUserRepositoryInterface $ssoUserRepository,
        IBL\Model\SsoUserFactory $ssoUserFactory,
        IBL\Api\SsoPermissionRepositoryInterface $ssoPermissionRepository
    ) {
        $this->userFactory = $userFactory;
        $this->userResource = $userResource;
        $this->adminLoginHelper = $adminLoginHelper;
        $this->resultFactory = $resultFactory;
        $this->ssoUserRepository = $ssoUserRepository;
        $this->ssoUserFactory = $ssoUserFactory;
        $this->ssoPermissionRepository = $ssoPermissionRepository;

        parent::__construct($context);
    }

    /**
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function findAdminUserByEmail(string $emailAddress): User\Model\User
    {
        /** @var User\Model\User $user */
        $user = $this->userFactory->create();
        $this->userResource->load($user, $emailAddress, 'email');

        if ($user->getId() === null) {
            throw new Framework\Exception\NoSuchEntityException(
                __('No admin user with e-mail address "%1"', $emailAddress)
            );
        }

        return $user;
    }

    /**
     * @throws \Exception
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    public function translateAdminUser(
        User\Model\User $user,
        string $firstName,
        string $lastName,
        string $emailAddress
    ): User\Model\User {
        $user
            ->setFirstName($firstName)
            ->setLastName($lastName)
            ->setEmail($emailAddress)
            ->setUserName($emailAddress)
            ->setIsActive(1)
            ->setPassword($this->generateRandomPassword());

        $this->userResource->save($user);

        return $user;
    }

    /**
     * @param IBL\Providers\ProviderInterface $provider,
     * @param User\Model\User $user
     * @throws \Exception
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Stdlib\Cookie\CookieSizeLimitReachedException
     * @throws \Magento\Framework\Stdlib\Cookie\FailureToSendException
     * @return Backend\Model\View\Result\Redirect
     */
    public function login(
        IBL\Providers\ProviderInterface $provider,
        User\Model\User $user
    ): Backend\Model\View\Result\Redirect {
        $this->ensureIsActive($provider);

        try {
            $ssoPermission = $this->ssoPermissionRepository->match($provider, $user);
        } catch (IBL\Exception\NoSuchSsoPermissionException $e) {
            throw new Framework\Exception\LocalizedException(
                __('This account is not authorized to log into the admin panel.')
            );
        }

        try {
            $user->setRoleId($ssoPermission->getAdminRoleId());
            $this->userResource->save($user);
        } catch (\Exception $e) {
            throw new Framework\Exception\LocalizedException(
                __('Unable to set the required permissions')
            );
        }

        if ($user->getId()) {
            $ssoUser = $this->getOrCreateSsoUser($user);

            $session = $this->adminLoginHelper->getAuthSession();
            $cookieManager = $this->adminLoginHelper->getCookieManager();

            // Login user
            $session->setUser($user);
            $session->processLogin();

            // Set cookie
            $cookieManager->setPublicCookie($session->getName(), $session->getSessionId());

            // Let events process the login
            $this->_eventManager->dispatch(
                'backend_auth_user_login_success',
                [
                    'user'     => $user,
                    'sso_user' => $ssoUser
                ]
            );

            // Perform after login actions
            $securityPlugin = $this->adminLoginHelper->getSecurityAuthPlugin();
            $securityPlugin->afterLogin($this->adminLoginHelper->getAuth());
        }

        /** @var \Magento\Backend\Model\View\Result\Redirect $redirect */
        $redirect = $this->resultFactory->create(Framework\Controller\ResultFactory::TYPE_REDIRECT);
        $redirect->setPath($this->adminLoginHelper->getBackendUrl()->getStartupPageUrl());

        return $redirect;
    }

    private function generateRandomPassword(): string
    {
        $gibberish = '';
        $chars = array_merge(range('a', 'z'), range('A', 'Z'), range(0, 9));

        for ($i = 0; $i < 64; $i++) {
            $gibberish .= (string)$chars[rand(0, count($chars) - 1)];
        }

        return $gibberish;
    }

    /**
     * @param \Magento\User\Api\Data\UserInterface|\Magento\User\Model\User $user
     *
     * @return IBL\Api\Data\SsoUserInterface|IBL\Model\SsoUser
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     * @throws \Exception
     */
    private function getOrCreateSsoUser(User\Model\User $user): IBL\Api\Data\SsoUserInterface
    {
        /** @var IBL\Api\Data\SsoUserInterface|IBL\Model\SsoUser $ssoUser */
        try {
            $ssoUser = $this->ssoUserRepository->getByAdminUser($user);
        } catch (Framework\Exception\NoSuchEntityException $e) {
            $ssoUser = $this->ssoUserFactory->create();
        }

        // Current datetime
        $now = new \DateTime('now');

        // Set first login if object is new
        if ($ssoUser->isObjectNew() === true) {
            $ssoUser->setFirstLoginAt($now);
        }

        // Update SSO user data
        $ssoUser
            ->setUser($user)
            ->setLastLoginAt($now)
            ->addLoginCount();

        // Save user
        $this->ssoUserRepository->save($ssoUser);

        return $ssoUser;
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function ensureIsActive(IBL\Providers\ProviderInterface $provider): void
    {
        if ($provider->isActive() !== true) {
            throw new Framework\Exception\LocalizedException(
                __('Chosen provider is not active')
            );
        }
    }

}