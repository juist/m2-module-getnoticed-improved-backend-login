<?php

namespace GetNoticed\ImprovedBackendLogin\Helper;

use Magento\Framework;
use GetNoticed\ImprovedBackendLogin as IBL;

class WhitelistDomainsHelper
    extends Framework\App\Helper\AbstractHelper
{

    // DI

    /**
     * @var Framework\Serialize\Serializer\Json
     */
    protected $jsonSerializer;

    /**
     * @var IBL\Api\Data\VO\WhitelistDomainObjectInterfaceFactory
     */
    protected $whitelistDomainObjectInterfaceFactory;

    public function __construct(
        Framework\App\Helper\Context $context,
        Framework\Serialize\Serializer\Json $jsonSerializer,
        IBL\Api\Data\VO\WhitelistDomainObjectInterfaceFactory $whitelistDomainObjectInterfaceFactory
    ) {
        $this->jsonSerializer = $jsonSerializer;
        $this->whitelistDomainObjectInterfaceFactory = $whitelistDomainObjectInterfaceFactory;

        parent::__construct($context);
    }

    /**
     * @return IBL\Api\Data\VO\WhitelistDomainObjectInterface[]|IBL\VO\WhitelistDomainObject[]
     */
    public function convertJsonToWhitelistDomainsObjects(string $jsonData): array
    {
        try {
            $unserialized = $this->jsonSerializer->unserialize($jsonData);
        } catch (\InvalidArgumentException $e) {
            return [];
        }

        $whitelistDomainObjects = [];

        foreach ($unserialized as $objectData) {
            $whitelistDomainObjects[] = $this->getWhitelistDomainObject()
                                             ->setHostname($objectData['hostname'])
                                             ->setTld($objectData['tld'])
                                             ->setUser($objectData['user']);
        }

        return $whitelistDomainObjects;
    }

    /**
     * @param IBL\Api\Data\VO\WhitelistDomainObjectInterface|IBL\VO\WhitelistDomainObject $object
     */
    public function getWhitelistDomainDescription(
        IBL\Api\Data\VO\WhitelistDomainObjectInterface $object
    ): string {
        if ($object->getUser() === '*') {
            return __('Everyone with an *@%1.%2 e-mail address', $object->getHostname(), $object->getTld());
        } else {
            return __(
                'User with e-mail address: %1@%2.%3', $object->getUser(), $object->getHostname(), $object->getTld()
            );
        }
    }

    /**
     * @return IBL\Api\Data\VO\WhitelistDomainObjectInterface|IBL\VO\WhitelistDomainObject
     */
    private function getWhitelistDomainObject(): IBL\Api\Data\VO\WhitelistDomainObjectInterface
    {
        return $this->whitelistDomainObjectInterfaceFactory->create();
    }

}