<?php

namespace GetNoticed\ImprovedBackendLogin\Helper\Config;

interface GeneralInterface
{

    const XML_PATH_BASE = 'getnoticed_improvedbackendlogin';

    const XML_PATH_GENERAL = self::XML_PATH_BASE . '/general';
    const XML_PATH_ENABLED = self::XML_PATH_GENERAL . '/enabled';

    public function isModuleEnabled(): bool;

}