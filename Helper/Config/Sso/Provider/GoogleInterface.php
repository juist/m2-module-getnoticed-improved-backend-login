<?php

namespace GetNoticed\ImprovedBackendLogin\Helper\Config\Sso\Provider;

use GetNoticed\ImprovedBackendLogin as IBL;

interface GoogleInterface
{

    const XML_PATH_GOOGLE = IBL\Helper\Config\GeneralInterface::XML_PATH_BASE . '/sso/provider_google';
    const XML_PATH_ACTIVE = self::XML_PATH_GOOGLE . '/active';
    const XML_PATH_APPLICATION_ID = self::XML_PATH_GOOGLE . '/application_id';
    const XML_PATH_APPLICATION_SECRET = self::XML_PATH_GOOGLE . '/application_secret';

    public function isActive(): bool;

    public function getApplicationId(): string;

    public function getApplicationSecret(): string;

    public function getRedirectUri(): string;

    public function getHostedDomain(): string;

}