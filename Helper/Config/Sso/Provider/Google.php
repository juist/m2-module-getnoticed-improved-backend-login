<?php

namespace GetNoticed\ImprovedBackendLogin\Helper\Config\Sso\Provider;

use Magento\Backend;
use Magento\Framework;
use Magento\Store;

/**
 * @method Framework\App\Request\Http _getRequest()
 */
class Google
    extends Framework\App\Helper\AbstractHelper
    implements GoogleInterface
{

    // DI

    /**
     * @var Backend\Model\UrlInterface
     */
    protected $urlBuilder;

    public function __construct(
        Framework\App\Helper\Context $context,
        Backend\Model\UrlInterface $urlBuilder
    ) {
        $this->urlBuilder = $urlBuilder;

        parent::__construct($context);
    }

    public function isActive(): bool
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_ACTIVE,
            Store\Model\ScopeInterface::SCOPE_STORES
        );
    }

    public function getApplicationId(): string
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_APPLICATION_ID,
            Store\Model\ScopeInterface::SCOPE_STORES
        );
    }

    public function getApplicationSecret(): string
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_APPLICATION_SECRET,
            Store\Model\ScopeInterface::SCOPE_STORES
        );
    }

    /**
     * We have to play it slightly ugly - Magento 2 refuses to generate backend URL's without a secret key.
     *
     * @return string
     */
    public function getRedirectUri(): string
    {
        return preg_replace(
            '#(/key/(.*)/)#',
            '/',
            $this->urlBuilder->getUrl('getnoticed/ibl_sso_provider_google/callback', ['key' => null])
        );
    }

    public function getHostedDomain(): string
    {
        return $this->_getRequest()->getServer('HTTP_HOST');
    }

}