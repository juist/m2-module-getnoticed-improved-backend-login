<?php

namespace GetNoticed\ImprovedBackendLogin\Helper\Config;

use Magento\Framework;
use Magento\Store;

class General
    extends Framework\App\Helper\AbstractHelper
    implements GeneralInterface
{

    public function isModuleEnabled(): bool
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_ENABLED,
            Store\Model\ScopeInterface::SCOPE_STORES
        );
    }

}