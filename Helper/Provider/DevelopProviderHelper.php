<?php

namespace GetNoticed\ImprovedBackendLogin\Helper\Provider;

use Magento\Framework;
use LayerShifter\TLDExtract;

class DevelopProviderHelper
    extends Framework\App\Helper\AbstractHelper
{

    // DI

    /**
     * @var TLDExtract\Extract
     */
    protected $tldExtract;

    public function __construct(
        Framework\App\Helper\Context $context
    ) {
        parent::__construct($context);

        $this->tldExtract = new TLDExtract\Extract();
    }


    public function getDevelopEmailAddress(): string
    {
        return sprintf(
            '%s@%s.%s',
            $this->getEmailUser(),
            $this->getEmailHostname(),
            $this->getEmailTld()
        );
    }

    public function getEmailUser(): string
    {
        return 'develop';
    }

    public function getEmailHostname(): string
    {
        return strtr(
            $this->tldExtract->parse($this->getUrlHostname())->getFullHost(),
            ['.' . $this->getEmailTld() => '']
        );
    }

    public function getEmailTld(): string
    {
        return $this->tldExtract->parse($this->getUrlHostname())->getSuffix();
    }

    private function getUrlHostname(): string
    {
        return \parse_url($this->_urlBuilder->getUrl(), PHP_URL_HOST);
    }

}