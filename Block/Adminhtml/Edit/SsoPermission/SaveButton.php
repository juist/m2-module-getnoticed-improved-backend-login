<?php

namespace GetNoticed\ImprovedBackendLogin\Block\Adminhtml\Edit\SsoPermission;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class SaveButton
 *
 * @package GetNoticed\ImprovedBackendLogin\Block\Adminhtml\Edit\SsoPermission
 */
class SaveButton
    extends AbstractButton
    implements ButtonProviderInterface
{

    /**
     * @return array
     */
    public function getButtonData()
    {
        $entityId = $this->getEntityId();
        $data = [];
        if ($entityId !== null) {
            $data = [
                'label'          => __('Save'),
                'class'          => 'save primary',
                'data_attribute' => [
                    'mage-init' => ['button' => ['event' => 'save']],
                    'form-role' => 'save',
                ],
                'sort_order'     => 90,
            ];
        } else {
            $data = [
                'label'          => __('Create'),
                'class'          => 'save primary',
                'data_attribute' => [
                    'mage-init' => ['button' => ['event' => 'save']],
                    'form-role' => 'save'
                ],
                'sort_order'     => 90
            ];
        }

        return $data;
    }
}