<?php

namespace GetNoticed\ImprovedBackendLogin\Block\Adminhtml\Edit\SsoPermission;

/**
 * Class AbstractButton
 *
 * @package GetNoticed\ImprovedBackendLogin\Block\Adminhtml\Edit\SsoPermission
 */
abstract class AbstractButton
{

    const CURRENT_ENTITY_ID = 'entity_id';

    /**
     * Url Builder
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    /**
     * Registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * Constructor
     *
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry           $registry
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry
    ) {
        $this->urlBuilder = $context->getUrlBuilder();
        $this->registry = $registry;
    }

    /**
     * Return the entity id.
     *
     * @return int|null
     */
    public function getEntityId()
    {
        return $this->registry->registry(self::CURRENT_ENTITY_ID);
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array  $params
     *
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->urlBuilder->getUrl($route, $params);
    }

}