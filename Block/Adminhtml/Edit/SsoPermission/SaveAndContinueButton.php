<?php

namespace GetNoticed\ImprovedBackendLogin\Block\Adminhtml\Edit\SsoPermission;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class SaveAndContinueButton
    extends AbstractButton
    implements ButtonProviderInterface
{

    /**
     * @return array
     */
    public function getButtonData()
    {
        $entityId = $this->getEntityId();
        $data = [];
        if ($entityId !== null) {
            $data = [
                'label'          => __('Save and Continue Edit'),
                'class'          => 'save',
                'data_attribute' => [
                    'mage-init' => [
                        'button' => ['event' => 'saveAndContinueEdit'],
                    ],
                ],
                'sort_order'     => 80,
            ];
        } else {
            $data = [
                'label'          => __('Create and Continue Edit'),
                'class'          => 'save',
                'data_attribute' => [
                    'mage-init' => [
                        'button' => ['event' => 'saveAndContinueEdit'],
                    ],
                ],
                'sort_order'     => 80,
            ];
        }

        return $data;
    }

}