<?php

namespace GetNoticed\ImprovedBackendLogin\Block\Adminhtml\Auth;

use Magento\Framework;
use Magento\Backend;
use GetNoticed\ImprovedBackendLogin as IBL;

class AltAuthMethods
    extends Framework\View\Element\Template
{

    // DI

    /**
     * @var IBL\Providers\ProviderListInterface
     */
    protected $providerList;

    /**
     * @var Backend\Model\UrlInterface
     */
    protected $adminUrl;

    public function __construct(
        IBL\Providers\ProviderListInterface $providerList,
        Backend\Model\UrlInterface $adminUrl,
        Framework\View\Element\Template\Context $context,
        array $data = []
    ) {
        $this->providerList = $providerList;
        $this->adminUrl = $adminUrl;

        parent::__construct($context, $data);
    }

    protected function _toHtml()
    {
        if (count($this->providerList->getActivatedProviders()) < 1) {
            return '';
        }

        return parent::_toHtml();
    }

    /**
     * @return IBL\Providers\ProviderInterface[]
     */
    public function getActiveProviders(): array
    {
        return $this->providerList->getActivatedProviders();
    }

}