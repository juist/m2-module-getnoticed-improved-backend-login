# GetNoticed Improved Backend Login README

## Installation

1. Include the module in your project using [Composer](https://getcomposer.org): `composer require getnoticed/module-improved-backend-login`
2. Enable the module: `php bin/magento module:enable GetNoticed_ImprovedBackendLogin`
3. Run setup:upgrade: `php bin/magento setup:upgrade`

**Note:** we're assuming you are working in developer mode locally and already execute the necessary commands when deploying an install. As such, commands such as `setup:di:compile` and `setup:static-content:deploy` are omitted on purpose.

## Upgrading

To upgrade, simply use Composer to install the latest version of this package and run `php bin/magento setup:upgrade`.

## Usage

For usage, please refer to the [User Guide](USER_GUIDE.pdf).