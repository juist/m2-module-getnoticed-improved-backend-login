<?php

namespace GetNoticed\ImprovedBackendLogin\Controller\Adminhtml\Ibl\Sso\Provider\Developer;

use Magento\Framework;
use Magento\Backend;
use Magento\User;
use GetNoticed\ImprovedBackendLogin as IBL;

/**
 * Note: we use a front-end controller on purpose: no need to be logged in as admin when logging in.
 *
 * @method \Magento\Framework\App\Request\Http getRequest()
 */
class Index
    extends Framework\App\Action\Action
{

    // DI

    /**
     * @var IBL\Helper\Config\GeneralInterface
     */
    protected $generalConfig;

    /**
     * @var IBL\Providers\Provider\DevelopProvider
     */
    protected $developProvider;

    /**
     * @var IBL\Helper\Provider\DevelopProviderHelper
     */
    protected $developProviderHelper;

    /**
     * @var Backend\Model\Session
     */
    protected $backendSession;

    /**
     * @var IBL\Helper\Sso\ExternalAdminUserHelper
     */
    protected $externalAdminUserHelper;

    /**
     * @var User\Model\UserFactory
     */
    protected $userFactory;

    public function __construct(
        Framework\App\Action\Context $context,
        IBL\Helper\Config\GeneralInterface $generalConfig,
        IBL\Providers\Provider\DevelopProvider $developProvider,
        IBL\Helper\Provider\DevelopProviderHelper $developProviderHelper,
        Backend\Model\Session $backendSession,
        IBL\Helper\Sso\ExternalAdminUserHelper $externalAdminUserHelper,
        User\Model\UserFactory $userFactory
    ) {
        $this->generalConfig = $generalConfig;
        $this->developProvider = $developProvider;
        $this->developProviderHelper = $developProviderHelper;
        $this->backendSession = $backendSession;
        $this->externalAdminUserHelper = $externalAdminUserHelper;
        $this->userFactory = $userFactory;

        parent::__construct($context);
    }

    public function execute()
    {
        try {
            $this->ensureIsActive();
        } catch (Framework\Exception\LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());

            return $this->redirectToAdminIndex();
        }

        // Find an admin user or if this fails, create an empty object
        $developerEmail = $this->developProviderHelper->getDevelopEmailAddress();
        $developerFirstName = 'Local';
        $developerLastName = 'Developer';

        try {
            $adminUser = $this->externalAdminUserHelper->findAdminUserByEmail($developerEmail);
        } catch (Framework\Exception\NoSuchEntityException $e) {
            /** @var User\Model\User $adminUser */
            $adminUser = $this->userFactory->create();
        }

        // Update the accounts information and save admin user (this will both create and/or update the admin user)
        try {
            $this->externalAdminUserHelper->translateAdminUser(
                $adminUser,
                $developerFirstName,
                $developerLastName,
                $developerEmail
            );
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $this->messageManager->addErrorMessage(
                __('Unable to save admin user, please try again later or contact us if this error persists.')
            );

            return $this->redirectToAdminIndex();
        }

        // At this point, we have a valid admin user, try to log them in.
        try {
            return $this->externalAdminUserHelper->login($this->developProvider, $adminUser);
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());

            return $this->redirectToAdminIndex();
        }
    }

    private function redirectToAdminIndex(): Framework\Controller\Result\Redirect
    {
        return $this->resultRedirectFactory->create()->setPath('admin');
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function ensureIsActive(): void
    {
        if ($this->generalConfig->isModuleEnabled() !== true) {
            throw new Framework\Exception\LocalizedException(
                __('SSO is not enabled - please configure the module in the admin first.')
            );
        }
    }

}