<?php

namespace GetNoticed\ImprovedBackendLogin\Controller\Adminhtml\Ibl\Sso\Provider\Google;

use Magento\Framework;
use Magento\Backend;
use Magento\User;
use GetNoticed\ImprovedBackendLogin as IBL;

/**
 * Note: we use a front-end controller on purpose: no need to be logged in as admin when authenticating for Google.
 * Also, the key can not be used as admin, because Google requires a non-changing redirect URL.
 *
 * @method \Magento\Framework\App\Request\Http getRequest()
 */
class Callback
    extends Framework\App\Action\Action
{

    // DI

    /**
     * @var IBL\Helper\Config\GeneralInterface
     */
    protected $generalConfig;

    /**
     * @var IBL\Helper\Config\Sso\Provider\GoogleInterface
     */
    protected $googleConfig;

    /**
     * @var IBL\Client\Provider\Google
     */
    protected $googleOAuthProvider;

    /**
     * @var IBL\Providers\Provider\GoogleProvider
     */
    protected $googleProvider;

    /**
     * @var Backend\Model\Session
     */
    protected $backendSession;

    /**
     * @var IBL\Helper\Sso\ExternalAdminUserHelper
     */
    protected $externalAdminUserHelper;

    /**
     * @var User\Model\UserFactory
     */
    protected $userFactory;

    public function __construct(
        Framework\App\Action\Context $context,
        IBL\Helper\Config\GeneralInterface $generalConfig,
        IBL\Helper\Config\Sso\Provider\GoogleInterface $googleConfig,
        IBL\Client\Provider\Google $googleOAuthProvider,
        IBL\Providers\Provider\GoogleProvider $googleProvider,
        Backend\Model\Session $backendSession,
        IBL\Helper\Sso\ExternalAdminUserHelper $externalAdminUserHelper,
        User\Model\UserFactory $userFactory
    ) {
        $this->generalConfig = $generalConfig;
        $this->googleConfig = $googleConfig;
        $this->googleOAuthProvider = $googleOAuthProvider;
        $this->googleProvider = $googleProvider;
        $this->backendSession = $backendSession;
        $this->externalAdminUserHelper = $externalAdminUserHelper;
        $this->userFactory = $userFactory;

        parent::__construct($context);
    }

    public function execute()
    {
        try {
            $this->ensureIsActive();
        } catch (Framework\Exception\LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());

            return $this->redirectToAdminIndex();
        }

        $error = $this->getRequest()->getParam('error');
        $errorEscaped = htmlspecialchars($error, ENT_QUOTES, 'UTF-8');
        $code = $this->getRequest()->getParam('code');
        $state = $this->getRequest()->getParam('state');

        if (!empty($error)) {
            // An error occured - show the error to the user and redirect to admin index.
            $this->messageManager->addErrorMessage(
                __('An error occured during Single Sign On authorization: %1', $errorEscaped)
            );

            return $this->redirectToAdminIndex();
        } elseif (empty($code)) {
            // No authorization code was found, redirect to Google login page to retrieve it.
            $authUrl = $this->googleOAuthProvider->getAuthorizationUrl();
            $this->backendSession->setGoogleProviderState($this->googleOAuthProvider->getState());

            $redirect = $this->resultRedirectFactory->create();
            $redirect->setUrl($authUrl);

            return $redirect;
        } elseif (empty($state) || $state !== $this->backendSession->getGoogleProviderState()) {
            // Invalid state detected - show error to user and redirect to admin index.
            $this->backendSession->unsGoogleProviderState();
            $this->messageManager->addErrorMessage(
                __('Invalid state - please retry')
            );

            return $this->redirectToAdminIndex();
        } else {
            // All information seems to be correct, get access token.
            try {
                $token = $this->googleOAuthProvider->getAccessToken('authorization_code', ['code' => $code]);
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());

                return $this->redirectToAdminIndex();
            }

            // Get the owner details so we can use these to create an admin user
            try {
                /** @var \League\OAuth2\Client\Provider\GoogleUser $ownerDetails */
                $ownerDetails = $this->googleOAuthProvider->getResourceOwner($token);
            } catch (\Exception | \Error $e) {
                $this->messageManager->addErrorMessage($e->getMessage());

                return $this->redirectToAdminIndex();
            }

            // Find an admin user or if this fails, create an empty object
            try {
                $adminUser = $this->externalAdminUserHelper->findAdminUserByEmail($ownerDetails->getEmail());
            } catch (Framework\Exception\NoSuchEntityException $e) {
                /** @var User\Model\User $adminUser */
                $adminUser = $this->userFactory->create();
            }

            // Update the accounts information and save admin user (this will both create and/or update the admin user)
            try {
                $this->externalAdminUserHelper->translateAdminUser(
                    $adminUser,
                    $ownerDetails->getFirstName(),
                    $ownerDetails->getLastName(),
                    $ownerDetails->getEmail()
                );
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                $this->messageManager->addErrorMessage(
                    __('Unable to save admin user, please try again later or contact us if this error persists.')
                );

                return $this->redirectToAdminIndex();
            }

            // At this point, we have a valid admin user, try to log them in.
            try {
                return $this->externalAdminUserHelper->login($this->googleProvider, $adminUser);
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());

                return $this->redirectToAdminIndex();
            }
        }
    }

    private function redirectToAdminIndex(): Framework\Controller\Result\Redirect
    {
        return $this->resultRedirectFactory->create()->setPath('admin');
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function ensureIsActive(): void
    {
        if ($this->generalConfig->isModuleEnabled() !== true) {
            throw new Framework\Exception\LocalizedException(
                __('SSO is not enabled - please configure the module in the admin first.')
            );
        }

        if ($this->googleConfig->isActive() !== true) {
            throw new Framework\Exception\LocalizedException(
                __('Google Provider is not enabled - please configure the provider in the admin first.')
            );
        }
    }

}