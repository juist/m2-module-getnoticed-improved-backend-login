<?php

namespace GetNoticed\ImprovedBackendLogin\Controller\Adminhtml\Ibl\Sso\Permissions;

use Magento\Backend\App\Action;
use Magento\Framework;
use Magento\Backend;
use GetNoticed\ImprovedBackendLogin as IBL;
use Psr\Log;
use LayerShifter\TLDExtract;

/**
 * @method Framework\App\Request\Http getRequest()
 */
class Save
    extends Backend\App\Action
{

    const ADMIN_RESOURCE = 'GetNoticed_ImprovedBackendLogin::sso_permissions';

    /**
     * @var bool
     */
    protected $returnToEdit = false;

    // DI

    /**
     * @var IBL\Model\SsoPermissionFactory
     */
    protected $permissionFactory;

    /**
     * @var IBL\Api\SsoPermissionRepositoryInterface
     */
    protected $permissionRepository;

    /**
     * @var IBL\Api\SsoPermissionServiceInterface
     */
    protected $permissionService;

    /**
     * @var Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var IBL\Api\Data\VO\WhitelistDomainObjectInterfaceFactory
     */
    protected $whitelistDomainObjectFactory;

    public function __construct(
        Backend\App\Action\Context $context,
        IBL\Model\SsoPermissionFactory $permissionFactory,
        IBL\Api\SsoPermissionRepositoryInterface $permissionRepository,
        IBL\Api\SsoPermissionServiceInterface $permissionService,
        Log\LoggerInterface $logger,
        IBL\Api\Data\VO\WhitelistDomainObjectInterfaceFactory $whitelistDomainObjectFactory
    ) {
        parent::__construct($context);

        $this->permissionFactory = $permissionFactory;
        $this->permissionRepository = $permissionRepository;
        $this->permissionService = $permissionService;
        $this->logger = $logger;
        $this->whitelistDomainObjectFactory = $whitelistDomainObjectFactory;
    }

    public function execute()
    {
        // Get data
        $data = $this->processData($this->getRequest()->getPostValue('sso_permission'));

        try {
            $permission = $this->permissionRepository->get($data['entity_id'] ?? 0);
        } catch (Framework\Exception\NoSuchEntityException | \TypeError $e) {
            $permission = $this->createNewPermission();
        }

        $currentDt = new \DateTime();
        $permission->addData($data);
        $permission->setCreatedAt($currentDt)->setUpdatedAt($currentDt);
        $permission->setWhitelistDomains(...$data['whitelist_domains']);

        try {
            // Validate
            $validateResult = $this->permissionService->validate($permission);

            if ($validateResult->isValid() === false) {
                throw new \Magento\Framework\Validator\Exception(
                    __('Failed saving the employer'),
                    null,
                    $validateResult->getMessages()
                );
            }

            $this->permissionRepository->save($permission);
            $this->_session->unsSsoPermissionFormData();

            $this->messageManager->addSuccessMessage(__('SSO permissions successfully saved.'));
            $this->returnToEdit = (bool)$this->getRequest()->getParam('back', false);
        } catch (Framework\Validator\Exception $e) {
            array_walk(
                $e->getMessages(),
                function (string $message) {
                    $this->messageManager->addErrorMessage($message);
                }
            );

            $this->_session->setSsoPermissionFormData($data);
            $this->returnToEdit = true;
        } catch (Framework\Exception\LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $this->_session->setSsoPermissionFormData($data);
            $this->returnToEdit = true;
        } catch (\Exception | \Error $e) {
            $this->messageManager->addErrorMessage(
                __('Unknown error during saving the permissions, error has been logged.')
            );
            $this->logger->critical($e->getMessage());
            $this->_session->setSsoPermissionFormData($data);
            $this->returnToEdit = true;
        }

        return $this->redirectToTarget($permission);
    }

    /**
     * @param IBL\Api\Data\SsoPermissionInterface|IBL\Model\SsoPermission $permission
     *
     * @return \Magento\Framework\App\Response\Http
     */
    protected function redirectToTarget(
        IBL\Api\Data\SsoPermissionInterface $permission
    ): Framework\App\Response\Http {
        if ($this->returnToEdit === true) {
            if ($permission->getId() !== null) {
                return $this->_redirect(
                    'getnoticed/ibl_sso_permissions/edit',
                    ['entity_id' => $permission->getId(), '_current' => true]
                );
            } else {
                return $this->_redirect(
                    'getnoticed/ibl_sso_permissions/create',
                    ['_current' => true]
                );
            }
        } else {
            return $this->_redirect('getnoticed/ibl_sso_permissions/index');
        }
    }

    /**
     * @return IBL\Api\Data\SsoPermissionInterface|IBL\Model\SsoPermission
     */
    protected function createNewPermission(): IBL\Api\Data\SsoPermissionInterface
    {
        return $this->permissionFactory->create();
    }

    /**
     * Process post data and modify where necessary to make it saveable.
     *
     * @param array $data
     *
     * @return array
     */
    private function processData(array $data): array
    {
        if (array_key_exists('whitelist_domains_text', $data)) {
            // Take the post value out of the array
            $post = $data['whitelist_domains_text'];
            unset($data['whitelist_domains_text']);

            // Set values to empty and process
            $extract = new TLDExtract\Extract();
            $whitelistDomains = [];

            foreach (explode(PHP_EOL, $post) as $line) {
                if (preg_match('/^(.*)@(.*)$/', $line, $user) !== 1) {
                    $this->messageManager->addNoticeMessage(
                        __('Removed the following line, because the syntax is invalid: %1', $line)
                    );
                    continue;
                }

                $userName = $user[1];
                $hostnameTld = $extract->parse($user[2]);
                $tld = $hostnameTld->getSuffix();
                $hostname = strtr($hostnameTld->getFullHost(), ['.' . $tld => '']);

                if (!empty($userName) && !empty($hostname) && !empty($tld)) {
                    $whitelistDomain = $this->whitelistDomainObjectFactory->create();
                    $whitelistDomain
                        ->setUser($userName)
                        ->setHostname($hostname)
                        ->setTld($tld);

                    $whitelistDomains[] = $whitelistDomain;
                }
            }

            $data['whitelist_domains'] = $whitelistDomains;
        }

        return $data;
    }

}