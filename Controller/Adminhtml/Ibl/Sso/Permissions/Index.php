<?php

namespace GetNoticed\ImprovedBackendLogin\Controller\Adminhtml\Ibl\Sso\Permissions;

use Magento\Framework;
use Magento\Backend;

class Index
    extends Backend\App\Action
{

    const ADMIN_RESOURCE = 'GetNoticed_ImprovedBackendLogin::sso_permissions';

    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $page */
        $page = $this->resultFactory->create(Framework\Controller\ResultFactory::TYPE_PAGE);
        $page->getConfig()->getTitle()->set(__('Manage SSO permissions'));

        return $page;
    }

}