<?php

namespace GetNoticed\ImprovedBackendLogin\Controller\Adminhtml\Ibl\Sso\Permissions;

use Magento\Framework;
use Magento\Backend;
use GetNoticed\ImprovedBackendLogin as IBL;

class Delete
    extends Backend\App\Action
{

    const ADMIN_RESOURCE = 'GetNoticed_ImprovedBackendLogin::sso_permissions';

    // DI

    /**
     * @var IBL\Api\SsoPermissionRepositoryInterface
     */
    protected $permissionRepository;

    public function __construct(
        Backend\App\Action\Context $context,
        IBL\Api\SsoPermissionRepositoryInterface $permissionRepository
    ) {
        parent::__construct($context);

        $this->permissionRepository = $permissionRepository;
    }

    public function execute()
    {
        if ($this->permissionRepository->delete($this->getSsoPermission()) !== true) {
            $this->messageManager->addErrorMessage(__('Failed deleting the permission.'));
        } else {
            $this->messageManager->addSuccessMessage(__('Permission was revoked and deleted successfully.'));
        }

        return $this->_redirect('getnoticed/ibl_sso_permissions/index');
    }

    /**
     * @return IBL\Api\Data\SsoPermissionInterface|IBL\Model\SsoPermission
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function getSsoPermission(): IBL\Api\Data\SsoPermissionInterface
    {
        $id = $this->getRequest()->getParam('entity_id');

        if ($id === null) {
            throw new \InvalidArgumentException('No ID given');
        }

        return $this->permissionRepository->get($id);
    }

}