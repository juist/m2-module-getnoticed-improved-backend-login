<?php

namespace GetNoticed\ImprovedBackendLogin\Controller\Adminhtml\Ibl\Sso\Permissions;

use Magento\Framework;
use Magento\Backend;

class Create
    extends Backend\App\Action
{

    const ADMIN_RESOURCE = 'GetNoticed_ImprovedBackendLogin::sso_permissions';

    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Forward $forward */
        $forward = $this->resultFactory->create(Framework\Controller\ResultFactory::TYPE_FORWARD);
        $forward->forward('edit');

        return $forward;
    }

}