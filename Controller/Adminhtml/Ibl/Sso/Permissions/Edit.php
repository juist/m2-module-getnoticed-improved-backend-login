<?php

namespace GetNoticed\ImprovedBackendLogin\Controller\Adminhtml\Ibl\Sso\Permissions;

use Magento\Framework;
use Magento\Backend;
use GetNoticed\ImprovedBackendLogin as IBL;

class Edit
    extends Backend\App\Action
{

    const ADMIN_RESOURCE = 'GetNoticed_ImprovedBackendLogin::sso_permissions';

    // DI

    /**
     * @var IBL\Api\SsoPermissionRepositoryInterface
     */
    protected $permissionRepository;

    public function __construct(
        Backend\App\Action\Context $context,
        IBL\Api\SsoPermissionRepositoryInterface $permissionRepository
    ) {
        parent::__construct($context);

        $this->permissionRepository = $permissionRepository;
    }

    public function execute()
    {
        /** @var \Magento\Framework\View\Result\Page $page */
        $page = $this->resultFactory->create(Framework\Controller\ResultFactory::TYPE_PAGE);

        try {
            $page->getConfig()->getTitle()->set(
                __(
                    'Change SSO permission: %1 (provider: %2, role: %3, %4 whitelisted domains)',
                    $this->getSsoPermission()->getId(),
                    $this->getSsoPermission()->getProvider()->getName(),
                    $this->getSsoPermission()->getAdminRole()->getRoleName(),
                    count($this->getSsoPermission()->getWhitelistDomains())
                )
            );
        } catch (Framework\Exception\NoSuchEntityException | \InvalidArgumentException $e) {
            $page->getConfig()->getTitle()->set(__('Create SSO permission'));
        }

        return $page;
    }

    /**
     * @return IBL\Api\Data\SsoPermissionInterface|IBL\Model\SsoPermission
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function getSsoPermission(): IBL\Api\Data\SsoPermissionInterface
    {
        $id = $this->getRequest()->getParam('entity_id');

        if ($id === null) {
            throw new \InvalidArgumentException('No ID given');
        }

        return $this->permissionRepository->get($id);
    }

}