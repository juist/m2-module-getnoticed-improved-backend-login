<?php

namespace GetNoticed\ImprovedBackendLogin\Ui\Component\Listing\Columns;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\UrlInterface;

/**
 * Class SsoPermissionActions
 *
 * @api
 */
class SsoPermissionActions
    extends Column
{

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @param ContextInterface   $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface       $urlBuilder
     * @param array              $components
     * @param array              $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     *
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $item[$this->getData('name')]['edit'] = [
                    'href'   => $this->urlBuilder->getUrl(
                        'getnoticed/ibl_sso_permissions/edit',
                        [
                            $this->getConfiguration()['indexField'] => $item['entity_id']
                        ]
                    ),
                    'label'  => __('Edit'),
                    'hidden' => false
                ];
                $item[$this->getData('name')]['delete'] = [
                    'href'    => $this->urlBuilder->getUrl(
                        'getnoticed/ibl_sso_permissions/delete',
                        [
                            $this->getConfiguration()['indexField'] => $item['entity_id']
                        ]
                    ),
                    'confirm' => [
                        'message' => __('Are you sure you want to delete this item?')
                    ],
                    'label'   => __('Delete'),
                    'hidden'  => false
                ];
            }
        }

        return $dataSource;
    }

}