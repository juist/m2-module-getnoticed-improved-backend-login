<?php

namespace GetNoticed\ImprovedBackendLogin\Ui\DataProvider\SsoPermission;

use GetNoticed\ImprovedBackendLogin as IBL;

/**
 * Class SsoPermissionDataProvider
 *
 * @api
 */
class SsoPermissionDataProvider
    extends \Magento\Ui\DataProvider\AbstractDataProvider
{

    /**
     * Product collection
     *
     * @var IBL\Model\ResourceModel\SsoPermission\Collection
     */
    protected $collection;

    /**
     * @var \Magento\Ui\DataProvider\AddFieldToCollectionInterface[]
     */
    protected $addFieldStrategies;

    /**
     * @var \Magento\Ui\DataProvider\AddFilterToCollectionInterface[]
     */
    protected $addFilterStrategies;

    // DI

    /**
     * @var IBL\Providers\ProviderListInterface
     */
    protected $providerList;

    /**
     * @var IBL\Helper\WhitelistDomainsHelper
     */
    protected $whitelistDomainsHelper;

    public function __construct(
        IBL\Providers\ProviderListInterface $providerList,
        IBL\Helper\WhitelistDomainsHelper $whitelistDomainsHelper,
        $name,
        $primaryFieldName,
        $requestFieldName,
        IBL\Model\ResourceModel\SsoPermission\CollectionFactory $collectionFactory,
        array $addFieldStrategies = [],
        array $addFilterStrategies = [],
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);

        $this->collection = $collectionFactory->create();
        $this->addFieldStrategies = $addFieldStrategies;
        $this->addFilterStrategies = $addFilterStrategies;

        // DI
        $this->providerList = $providerList;
        $this->whitelistDomainsHelper = $whitelistDomainsHelper;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (!$this->getCollection()->isLoaded()) {
            $this->getCollection()->load();
        }

        array_map([$this, 'formatEntity'], $this->getCollection()->getItems());

        return $this->getCollection()->toArray();
    }

    /**
     * Add field to select
     *
     * @param string|array $field
     * @param string|null  $alias
     *
     * @return void
     */
    public function addField($field, $alias = null)
    {
        if (isset($this->addFieldStrategies[$field])) {
            $this->addFieldStrategies[$field]->addField($this->getCollection(), $field, $alias);
        } else {
            parent::addField($field, $alias);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function addFilter(\Magento\Framework\Api\Filter $filter)
    {
        if (isset($this->addFilterStrategies[$filter->getField()])) {
            $this->addFilterStrategies[$filter->getField()]
                ->addFilter(
                    $this->getCollection(),
                    $filter->getField(),
                    [$filter->getConditionType() => $filter->getValue()]
                );
        } else {
            parent::addFilter($filter);
        }
    }

    /**
     * @param IBL\Api\Data\SsoPermissionInterface|IBL\Model\SsoPermission $ssoPermission
     *
     * @return IBL\Api\Data\SsoPermissionInterface|IBL\Model\SsoPermission $ssoPermission
     */
    private function formatEntity(
        IBL\Api\Data\SsoPermissionInterface $ssoPermission
    ): IBL\Api\Data\SsoPermissionInterface {
        // Set provider
        try {
            $ssoPermission->setData('provider', $ssoPermission->getProvider()->getName());
        } catch (\Exception | \Error $e) {
            $ssoPermission->setData('provider', $ssoPermission->getProviderCode());
        }

        // Set admin role
        try {
            $ssoPermission->setData('admin_role', $ssoPermission->getAdminRole()->getRoleName());
        } catch (\Exception | \Error $e) {
            $ssoPermission->setData('admin_role', __('Role deleted: %1', $ssoPermission->getAdminRoleId()));
        }

        // Format whitelist domain list
        if (count($ssoPermission->getWhitelistDomains()) > 0) {
            $listData = [];

            foreach ($ssoPermission->getWhitelistDomains() as $whitelistDomain) {
                $listData[] = $this->whitelistDomainsHelper->getWhitelistDomainDescription($whitelistDomain);
            }

            if (count($listData) > 2) {
                $ssoPermission->setData('whitelist_domains_list', __('%1 whitelisted users/groups', count($listData)));
            } else {
                $ssoPermission->setData('whitelist_domains_list', implode('; ', $listData));
            }
        } else {
            $ssoPermission->setData('whitelist_domains_list', '-');
        }

        // Return data
        return $ssoPermission;
    }

}