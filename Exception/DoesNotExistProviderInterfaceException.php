<?php

namespace GetNoticed\ImprovedBackendLogin\Exception;

class DoesNotExistProviderInterfaceException
    extends AbstractException
{
}