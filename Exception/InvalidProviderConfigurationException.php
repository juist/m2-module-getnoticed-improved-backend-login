<?php

namespace GetNoticed\ImprovedBackendLogin\Exception;

class InvalidProviderConfigurationException
    extends AbstractException
{
}