<?php

namespace GetNoticed\ImprovedBackendLogin\Exception;

use Magento\Framework\Exception;

abstract class AbstractException
    extends Exception\LocalizedException
{
}