<?php

namespace GetNoticed\ImprovedBackendLogin\Exception;

class NoSuchSsoPermissionException
    extends AbstractException
{
}