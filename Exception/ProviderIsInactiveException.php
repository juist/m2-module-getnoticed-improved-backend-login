<?php

namespace GetNoticed\ImprovedBackendLogin\Exception;

class ProviderIsInactiveException
    extends AbstractException
{
}