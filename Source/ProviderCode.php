<?php

namespace GetNoticed\ImprovedBackendLogin\Source;

use GetNoticed\ImprovedBackendLogin as IBL;
use Magento\Framework;

class ProviderCode
    implements Framework\Option\ArrayInterface
{

    // DI

    /**
     * @var IBL\Providers\ProviderListInterface
     */
    protected $providerList;

    public function __construct(
        IBL\Providers\ProviderListInterface $providerList
    ) {
        $this->providerList = $providerList;
    }

    /**
     * @inheritDoc
     */
    public function toOptionArray()
    {
        return array_map(
            function (IBL\Providers\ProviderInterface $provider) {
                return [
                    'value' => $provider->getCode(),
                    'label' => $provider->getName()
                ];
            },
            $this->providerList->getActivatedProviders()
        );
    }

}