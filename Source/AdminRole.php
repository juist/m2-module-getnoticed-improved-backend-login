<?php

namespace GetNoticed\ImprovedBackendLogin\Source;

use Magento\Framework;
use Magento\Authorization;

class AdminRole
    implements Framework\Option\ArrayInterface
{

    // DI

    /**
     * @var Authorization\Model\ResourceModel\Role\CollectionFactory
     */
    protected $roleCollectionFactory;

    public function __construct(
        Authorization\Model\ResourceModel\Role\CollectionFactory $roleCollectionFactory
    ) {
        $this->roleCollectionFactory = $roleCollectionFactory;
    }

    /**
     * @inheritDoc
     */
    public function toOptionArray()
    {
        return array_map(
            function (Authorization\Model\Role $role) {
                return [
                    'value' => $role->getId(),
                    'label' => $role->getRoleName()
                ];
            },
            $this->getRoleCollection()->getItems()
        );
    }

    /**
     * @return \Magento\Authorization\Model\ResourceModel\Role\Collection
     */
    protected function getRoleCollection(): \Magento\Authorization\Model\ResourceModel\Role\Collection
    {
        $collection = $this->roleCollectionFactory->create();
        $collection->addFieldToFilter('role_type', ['eq' => Authorization\Model\Acl\Role\Group::ROLE_TYPE]);

        return $collection;
    }

}