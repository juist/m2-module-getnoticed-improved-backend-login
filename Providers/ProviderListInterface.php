<?php

namespace GetNoticed\ImprovedBackendLogin\Providers;

use GetNoticed\ImprovedBackendLogin as IBL;

interface ProviderListInterface
{

    /**
     * @return IBL\Providers\ProviderInterface[]
     */
    public function getProviders(): array;

    /**
     * @return IBL\Providers\ProviderInterface[]
     */
    public function getActivatedProviders(): array;

    /**
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getProviderByCode(string $providerCode): IBL\Providers\ProviderInterface;

}