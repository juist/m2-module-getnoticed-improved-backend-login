<?php

namespace GetNoticed\ImprovedBackendLogin\Providers;

interface ProviderInterface
{

    public function getCode(): string;

    public function getName(): string;

    public function isActive(): bool;

    public function getStartSingleSignOnUrl(): string;

}