<?php

namespace GetNoticed\ImprovedBackendLogin\Providers\Provider;

use Magento\Framework;
use Magento\Backend;
use GetNoticed\ImprovedBackendLogin as IBL;

class GoogleProvider
    implements IBL\Providers\ProviderInterface
{

    const CODE = 'google';
    const NAME = 'Google';

    /**
     * @var IBL\Helper\Config\GeneralInterface
     */
    protected $generalConfig;

    /**
     * @var IBL\Helper\Config\Sso\Provider\GoogleInterface
     */
    protected $googleConfig;

    /**
     * @var Backend\Model\UrlInterface
     */
    protected $backendUrl;

    /**
     * @var Framework\App\State
     */
    protected $appState;

    public function __construct(
        IBL\Helper\Config\GeneralInterface $generalConfig,
        IBL\Helper\Config\Sso\Provider\GoogleInterface $googleConfig,
        Backend\Model\UrlInterface $backendUrl,
        Framework\App\State $appState
    ) {
        $this->generalConfig = $generalConfig;
        $this->googleConfig = $googleConfig;
        $this->backendUrl = $backendUrl;
        $this->appState = $appState;
    }

    public function getCode(): string
    {
        return self::CODE;
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function isActive(): bool
    {
        return $this->generalConfig->isModuleEnabled()
            && $this->appState->getMode() !== Framework\App\State::MODE_DEVELOPER && $this->googleConfig->isActive();
    }

    public function getStartSingleSignOnUrl(): string
    {
        return $this->backendUrl->getUrl('getnoticed/ibl_sso_provider_google/callback');
    }

}