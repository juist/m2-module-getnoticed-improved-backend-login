<?php

namespace GetNoticed\ImprovedBackendLogin\Providers\Provider;

use Magento\Framework;
use Magento\Backend;
use GetNoticed\ImprovedBackendLogin as IBL;

class DevelopProvider
    implements IBL\Providers\ProviderInterface
{

    const CODE = 'develop';
    const NAME = 'Develop';

    /**
     * @var IBL\Helper\Config\GeneralInterface
     */
    protected $generalConfig;

    /**
     * @var Backend\Model\UrlInterface
     */
    protected $backendUrl;

    /**
     * @var Framework\App\State
     */
    protected $appState;

    public function __construct(
        IBL\Helper\Config\GeneralInterface $generalConfig,
        Backend\Model\UrlInterface $backendUrl,
        Framework\App\State $appState
    ) {
        $this->generalConfig = $generalConfig;
        $this->backendUrl = $backendUrl;
        $this->appState = $appState;
    }

    public function getCode(): string
    {
        return self::CODE;
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function isActive(): bool
    {
        return $this->generalConfig->isModuleEnabled()
            && $this->appState->getMode() === Framework\App\State::MODE_DEVELOPER;
    }

    public function getStartSingleSignOnUrl(): string
    {
        return $this->backendUrl->getUrl('getnoticed/ibl_sso_provider_developer');
    }

}