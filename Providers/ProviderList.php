<?php

namespace GetNoticed\ImprovedBackendLogin\Providers;

use Magento\Framework;
use GetNoticed\ImprovedBackendLogin as IBL;

class ProviderList
    implements ProviderListInterface
{

    /**
     * @var IBL\Providers\ProviderInterface[]
     */
    protected $providers = [];

    /**
     * @param IBL\Providers\ProviderInterface[] $providers
     *
     * @throws IBL\Exception\DoesNotExistProviderInterfaceException
     */
    public function __construct(
        array $providers = []
    ) {
        $this->providers = $this->ensureCorrectType($providers);
    }

    /**
     * @inheritdoc
     * @return IBL\Providers\ProviderInterface[]
     */
    public function getProviders(): array
    {
        return $this->providers;
    }

    /**
     * @inheritdoc
     */
    public function getActivatedProviders(): array
    {
        $activeProviders = [];

        foreach ($this->providers as $key => $provider) {
            if ($provider->isActive()) {
                $activeProviders[$key] = $provider;
            }
        }

        return $activeProviders;
    }

    /**
     * @inheritdoc
     */
    public function getProviderByCode(string $providerCode): IBL\Providers\ProviderInterface
    {
        foreach ($this->getProviders() as $provider) {
            if ($provider->getCode() === $providerCode) {
                return $provider;
            }
        }

        throw new Framework\Exception\NoSuchEntityException(
            __('No provider found with code "%1"', $providerCode)
        );
    }

    /**
     * @param IBL\Providers\ProviderInterface[] $providers
     *
     * @return IBL\Providers\ProviderInterface[]
     * @throws IBL\Exception\DoesNotExistProviderInterfaceException
     */
    private function ensureCorrectType(
        array $providers = []
    ): array {
        foreach ($providers as $provider) {
            if (!$provider instanceof ProviderInterface) {
                throw new IBL\Exception\DoesNotExistProviderInterfaceException(
                    __(
                        'Provider class "%1" does not exist %2',
                        get_class($provider),
                        ProviderInterface::class
                    )
                );
            }
        }

        return $providers;
    }

}