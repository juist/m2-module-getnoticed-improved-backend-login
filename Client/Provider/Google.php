<?php

namespace GetNoticed\ImprovedBackendLogin\Client\Provider;

use League\OAuth2;
use GetNoticed\ImprovedBackendLogin as IBL;

class Google
    extends OAuth2\Client\Provider\Google
{

    /**
     * @param IBL\Helper\Config\Sso\Provider\GoogleInterface|GetNoticed\ImprovedBackendLogin\Helper\Config\Sso\Provider\Google
     * @throws IBL\Exception\ProviderIsInactiveException
     * @throws IBL\Exception\InvalidProviderConfigurationException
     */
    public function __construct(
        IBL\Helper\Config\Sso\Provider\GoogleInterface $googleConfig
    ) {
        if ($googleConfig->isActive() === false) {
            throw new IBL\Exception\ProviderIsInactiveException(
                __('%1 provider is not enabled', 'Google')
            );
        }

        try {
            $options = [
                'clientId'     => $googleConfig->getApplicationId(),
                'clientSecret' => $googleConfig->getApplicationSecret(),
                'redirectUri'  => $googleConfig->getRedirectUri()
            ];

            parent::__construct($options, []);
        } catch (\Exception | \Error $e) {
            throw new IBL\Exception\InvalidProviderConfigurationException(
                __('Configuration was incomplete, please check backend configuration.')
            );
        }
    }

}