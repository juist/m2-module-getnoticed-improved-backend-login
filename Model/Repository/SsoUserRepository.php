<?php

namespace GetNoticed\ImprovedBackendLogin\Model\Repository;

use Magento\Framework;
use Magento\User;
use GetNoticed\ImprovedBackendLogin as IBL;

class SsoUserRepository
    implements IBL\Api\SsoUserRepositoryInterface
{

    // DI

    /**
     * @var IBL\Model\SsoUserFactory
     */
    protected $factory;

    /**
     * @var IBL\Model\ResourceModel\SsoUser
     */
    protected $resource;

    /**
     * IBL\Model\ResourceModel\SsoUser\Collection
     */
    protected $collectionFactory;

    public function __construct(
        IBL\Model\SsoUserFactory $factory,
        IBL\Model\ResourceModel\SsoUser $resource,
        IBL\Model\ResourceModel\SsoUser\Collection $collectionFactory
    ) {
        $this->factory = $factory;
        $this->resource = $resource;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @inheritdoc
     */
    public function getByAdminUser(User\Api\Data\UserInterface $adminUser): IBL\Api\Data\SsoUserInterface
    {
        $entity = $this->getEntity();
        $this->resource->load($entity, $adminUser->getId(), IBL\Api\Data\SsoUserInterface::KEY_USER_ID);

        if ($entity->getId() === null) {
            throw new Framework\Exception\NoSuchEntityException(
                __('No SSO user found for admin user "%1"', $adminUser->getId())
            );
        }

        return $entity;
    }

    /**
     * @inheritdoc
     *
     * @param IBL\Api\Data\SsoUserInterface|IBL\Model\SsoUser $ssoUser
     */
    public function save(IBL\Api\Data\SsoUserInterface $ssoUser): IBL\Api\Data\SsoUserInterface
    {
        $this->resource->save($ssoUser);

        return $ssoUser;
    }

    /**
     * @inheritdoc
     *
     * @param IBL\Api\Data\SsoUserInterface|IBL\Model\SsoUser $ssoUser
     */
    public function delete(IBL\Api\Data\SsoUserInterface $ssoUser): bool
    {
        try {
            $this->resource->delete($ssoUser);

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @return IBL\Api\Data\SsoUserInterface|IBL\Model\SsoUser
     */
    private function getEntity(): IBL\Api\Data\SsoUserInterface
    {
        return $this->factory->create();
    }

}