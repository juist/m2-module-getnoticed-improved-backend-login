<?php

namespace GetNoticed\ImprovedBackendLogin\Model\Repository;

use Magento\Framework;
use Magento\User;
use GetNoticed\ImprovedBackendLogin as IBL;

class SsoPermissionRepository
    implements IBL\Api\SsoPermissionRepositoryInterface
{

    const PATTERN_MATCH_EMAIL_ADDRESS_DOMAIN = '#^(.*)@%1$s.%2$s$#';

    /**
     * @var IBL\Model\SsoPermissionFactory
     */
    protected $factory;

    /**
     * @var IBL\Model\ResourceModel\SsoPermission
     */
    protected $resource;

    /**
     * @var IBL\Model\ResourceModel\SsoPermission\CollectionFactory
     */
    protected $collectionFactory;

    public function __construct(
        IBL\Model\SsoPermissionFactory $factory,
        IBL\Model\ResourceModel\SsoPermission $resource,
        IBL\Model\ResourceModel\SsoPermission\CollectionFactory $collectionFactory
    ) {
        $this->factory = $factory;
        $this->resource = $resource;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @inheritdoc
     */
    public function get(int $entityId): IBL\Api\Data\SsoPermissionInterface
    {
        $entity = $this->getEntity();
        $this->resource->load($entity, $entityId);

        if ($entity->getId() === null) {
            throw new Framework\Exception\NoSuchEntityException(
                __('No SSO permission with entity ID "%1" found', $entityId)
            );
        }

        return $entity;
    }

    /**
     * @inheritdoc
     *
     * @param IBL\Api\Data\SsoPermissionInterface|IBL\Model\SsoPermission $ssoPermission
     */
    public function save(IBL\Api\Data\SsoPermissionInterface $ssoPermission): IBL\Api\Data\SsoPermissionInterface
    {
        $this->resource->save($ssoPermission);

        return $ssoPermission;
    }

    /**
     * @inheritdoc
     *
     * @param IBL\Api\Data\SsoPermissionInterface|IBL\Model\SsoPermission $ssoPermission
     */
    public function delete(IBL\Api\Data\SsoPermissionInterface $ssoPermission): bool
    {
        try {
            $this->resource->delete($ssoPermission);

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function match(
        IBL\Providers\ProviderInterface $provider,
        User\Api\Data\UserInterface $user
    ): IBL\Api\Data\SsoPermissionInterface {
        $permissions = $this->getCollection();
        $permissions->addFieldToFilter(
            IBL\Api\Data\SsoPermissionInterface::KEY_PROVIDER_CODE,
            ['eq' => $provider->getCode()]
        );

        foreach ($permissions as $permission) {
            /** @var IBL\Api\Data\SsoPermissionInterface|IBL\Model\SsoPermission $permission */
            foreach ($permission->getWhitelistDomains() as $whitelistDomain) {
                if ($whitelistDomain->getUser() === '*') {
                    $whitelistDomainEmailRegex = sprintf(
                        self::PATTERN_MATCH_EMAIL_ADDRESS_DOMAIN,
                        $whitelistDomain->getHostname(),
                        $whitelistDomain->getTld()
                    );

                    if (preg_match($whitelistDomainEmailRegex, $user->getEmail()) === 1) {
                        return $permission;
                    }
                } else {
                    $whitelistDomainEmail = sprintf(
                        '%s@%s.%s',
                        $whitelistDomain->getUser(),
                        $whitelistDomain->getHostname(),
                        $whitelistDomain->getTld()
                    );

                    if ($user->getEmail() === $whitelistDomainEmail) {
                        return $permission;
                    }
                }
            }
        }

        throw new IBL\Exception\NoSuchSsoPermissionException(
            __('No permission found - access for user is denied.')
        );
    }

    /**
     * @return IBL\Api\Data\SsoPermissionInterface|IBL\Model\SsoPermission
     */
    private function getEntity(): IBL\Api\Data\SsoPermissionInterface
    {
        return $this->factory->create();
    }

    private function getCollection(): IBL\Model\ResourceModel\SsoPermission\Collection
    {
        return $this->collectionFactory->create();
    }

}