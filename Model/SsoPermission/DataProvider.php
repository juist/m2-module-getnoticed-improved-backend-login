<?php

namespace GetNoticed\ImprovedBackendLogin\Model\SsoPermission;

use GetNoticed\ImprovedBackendLogin as IBL;

class DataProvider
    extends \Magento\Ui\DataProvider\AbstractDataProvider
{

    /**
     * @var \GetNoticed\ImprovedBackendLogin\Model\ResourceModel\SsoPermission\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var \Magento\Backend\Model\Session
     */
    protected $session;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * @var \Magento\Framework\Serialize\Serializer\Json $jsonSerializer
     */
    protected $jsonSerializer;

    public function __construct(
        \Magento\Framework\Serialize\Serializer\Json $jsonSerializer,
        \Magento\Backend\Model\Session $session,
        \GetNoticed\ImprovedBackendLogin\Model\ResourceModel\SsoPermission\CollectionFactory $collectionFactory,
        $name,
        $primaryFieldName,
        $requestFieldName,
        array $meta = [],
        array $data = []
    ) {
        $this->jsonSerializer = $jsonSerializer;
        $this->session = $session;
        $this->collection = $collectionFactory->create();

        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);

    }

    /**
     * @return array
     */
    public function getData()
    {
        if ($this->loadedData === null) {
            $this->loadedData = [];

            foreach ($this->getCollection()->getItems() as $item) {
                /** @var \GetNoticed\ImprovedBackendLogin\Model\SsoPermission $item */
                $this->loadedData[$item->getId()] = [
                    'sso_permission' => array_merge(
                        $item->getData(),
                        [
                            'id_field_name'          => $item->getIdFieldName(),
                            'whitelist_domains_text' => implode(
                                PHP_EOL,
                                array_map(
                                    function (IBL\Api\Data\VO\WhitelistDomainObjectInterface $whitelist) {
                                        return sprintf(
                                            '%s@%s.%s',
                                            $whitelist->getUser(),
                                            $whitelist->getHostname(),
                                            $whitelist->getTld()
                                        );
                                    },
                                    $item->getWhitelistDomains()
                                )
                            )
                        ]
                    )
                ];
            }
        }

        $formData = $this->session->getSsoPermissionFormData();
        if (!empty($formData)) {
            $entityId = isset($data['sso_permission']['entity_id']) ? $formData['sso_permission']['entity_id'] : null;
            $this->loadedData[$entityId] = $formData;
            $this->session->unsSsoPermissionFormData();
        }

        return $this->loadedData;
    }

}