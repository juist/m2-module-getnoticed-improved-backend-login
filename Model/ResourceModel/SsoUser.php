<?php

namespace GetNoticed\ImprovedBackendLogin\Model\ResourceModel;

use Magento\Framework;
use GetNoticed\ImprovedBackendLogin as IBL;

class SsoUser
    extends Framework\Model\ResourceModel\Db\AbstractDb
{

    protected function _construct()
    {
        $this->_init(IBL\Api\Data\SsoUserInterface::TABLE_NAME, IBL\Api\Data\SsoUserInterface::PRIMARY_KEY_FIELD);
    }

}