<?php

namespace GetNoticed\ImprovedBackendLogin\Model\ResourceModel\SsoPermission;

use Magento\Framework;
use GetNoticed\ImprovedBackendLogin as IBL;

class Collection
    extends Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    protected $_idFieldName = IBL\Api\Data\SsoPermissionInterface::PRIMARY_KEY_FIELD;
    protected $_eventPrefix = 'getnoticed_improvedbackendlogin_ssopermission_collection';
    protected $_eventObject = 'getnoticed_improvedbackendlogin_ssopermission';

    protected function _construct()
    {
        $this->_init(
            IBL\Model\SsoPermission::class,
            IBL\Model\ResourceModel\SsoPermission::class
        );
    }

}