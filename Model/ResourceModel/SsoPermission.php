<?php

namespace GetNoticed\ImprovedBackendLogin\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use GetNoticed\ImprovedBackendLogin\Api\Data\SsoPermissionInterface;

class SsoPermission extends AbstractDb
{
    protected function _construct()
    {
        $this->_init(SsoPermissionInterface::TABLE_NAME, SsoPermissionInterface::PRIMARY_KEY_FIELD);
    }
}
