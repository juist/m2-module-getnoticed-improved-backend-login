<?php

namespace GetNoticed\ImprovedBackendLogin\Model\ResourceModel\SsoUser;

use Magento\Framework;
use GetNoticed\ImprovedBackendLogin as IBL;

class Collection
    extends Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    protected $_idFieldName = IBL\Api\Data\SsoUserInterface::PRIMARY_KEY_FIELD;

    protected function _construct()
    {
        $this->_init(
            IBL\Model\SsoUser::class,
            IBL\Model\ResourceModel\SsoUser::class
        );
    }

}