<?php

namespace GetNoticed\ImprovedBackendLogin\Model;

use Magento\Framework;
use Magento\User;
use GetNoticed\ImprovedBackendLogin as IBL;

/**
 * @method IBL\Model\ResourceModel\SsoUser getResource()
 * @method IBL\Model\ResourceModel\SsoUser\Collection getCollection()
 */
class SsoUser
    extends Framework\Model\AbstractModel
    implements IBL\Api\Data\SsoUserInterface,
               Framework\DataObject\IdentityInterface
{

    // External objects

    /**
     * @var \Magento\User\Api\Data\UserInterface|\Magento\User\Model\User
     */
    protected $user;

    // Magento model settings

    const CACHE_TAG = 'getnoticed_improvedbackendlogin_ssouser';

    protected $_cacheTag = 'getnoticed_improvedbackendlogin_ssouser';
    protected $_eventPrefix = 'getnoticed_improvedbackendlogin_ssouser';

    protected function _construct()
    {
        $this->_init(IBL\Model\ResourceModel\SsoUser::class);
    }

    /**
     * @inheritdoc
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @inheritdoc
     */
    public function getUser(): User\Api\Data\UserInterface
    {
        return $this->user;
    }

    public function getUserId(): int
    {
        return $this->getData(IBL\Api\Data\SsoUserInterface::KEY_USER_ID);
    }

    /**
     * @inheritdoc
     */
    public function setUser(User\Api\Data\UserInterface $user): IBL\Api\Data\SsoUserInterface
    {
        $this->user = $user;

        return $this->setData(IBL\Api\Data\SsoUserInterface::KEY_USER_ID, $user->getId());
    }

    /**
     * @inheritdoc
     */
    public function getFirstLoginAt(): \DateTimeInterface
    {
        return new \DateTime($this->getData(IBL\Api\Data\SsoUserInterface::KEY_FIRST_LOGIN_AT));
    }

    /**
     * @inheritdoc
     */
    public function setFirstLoginAt(\DateTimeInterface $firstLoginAt): IBL\Api\Data\SsoUserInterface
    {
        return $this->setData(IBL\Api\Data\SsoUserInterface::KEY_FIRST_LOGIN_AT, $firstLoginAt->format('Y-m-d H:i:s'));
    }

    /**
     * @inheritdoc
     */
    public function getLastLoginAt(): \DateTimeInterface
    {
        return new \DateTime($this->getData(IBL\Api\Data\SsoUserInterface::KEY_LAST_LOGIN_AT));
    }

    /**
     * @inheritdoc
     */
    public function setLastLoginAt(\DateTimeInterface $lastLoginAt): IBL\Api\Data\SsoUserInterface
    {
        return $this->setData(IBL\Api\Data\SsoUserInterface::KEY_LAST_LOGIN_AT, $lastLoginAt->format('Y-m-d H:i:s'));
    }

    /**
     * @inheritdoc
     */
    public function getLoginCount(): int
    {
        return $this->getData(IBL\Api\Data\SsoUserInterface::KEY_LOGIN_COUNT) ?: 0;
    }

    /**
     * @inheritdoc
     */
    public function setLoginCount(int $loginCount): IBL\Api\Data\SsoUserInterface
    {
        return $this->setData(IBL\Api\Data\SsoUserInterface::KEY_LOGIN_COUNT, $loginCount);
    }

    /**
     * @inheritdoc
     */
    public function addLoginCount(): IBL\Api\Data\SsoUserInterface
    {
        return $this->setLoginCount($this->getLoginCount() + 1);
    }

}