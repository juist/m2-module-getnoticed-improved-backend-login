<?php

namespace GetNoticed\ImprovedBackendLogin\Model;

use Magento\Framework;
use Magento\Authorization;
use GetNoticed\ImprovedBackendLogin as IBL;

/**
 * @method IBL\Model\ResourceModel\SsoPermission getResource()
 * @method IBL\Model\ResourceModel\SsoPermission\Collection getCollection()
 */
class SsoPermission
    extends Framework\Model\AbstractModel
    implements IBL\Api\Data\SsoPermissionInterface,
               Framework\DataObject\IdentityInterface
{

    // External objects

    /**
     * @var IBL\Providers\ProviderInterface
     */
    protected $provider;

    /**
     * @var \Magento\Authorization\Model\Role
     */
    protected $adminRole;

    /**
     * @var iBL\Api\Data\VO\WhitelistDomainObjectInterface[]|iBL\VO\WhitelistDomainObject[]
     */
    protected $whitelistDomains = [];

    // Magento model settings

    const CACHE_TAG = 'getnoticed_improvedbackendlogin_ssopermission';

    protected $_cacheTag = 'getnoticed_improvedbackendlogin_ssopermission';
    protected $_eventPrefix = 'getnoticed_improvedbackendlogin_ssopermission';

    protected function _construct()
    {
        $this->_init(IBL\Model\ResourceModel\SsoPermission::class);
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @inheritdoc
     */
    public function getProviderCode(): string
    {
        return $this->getData(IBL\Api\Data\SsoPermissionInterface::KEY_PROVIDER_CODE);
    }

    /**
     * @inheritdoc
     */
    public function setProviderCode(string $providerCode): IBL\Api\Data\SsoPermissionInterface
    {
        return $this->setData(IBL\Api\Data\SsoPermissionInterface::KEY_PROVIDER_CODE, $providerCode);
    }

    /**
     * @inheritdoc
     */
    public function getProvider(): IBL\Providers\ProviderInterface
    {
        return $this->provider;
    }

    /**
     * @inheritdoc
     */
    public function setProvider(IBL\Providers\ProviderInterface $provider): IBL\Api\Data\SsoPermissionInterface
    {
        $this->provider = $provider;

        return $this->setProviderCode($provider->getCode());
    }

    /**
     * @inheritdoc
     */
    public function getAdminRoleId(): int
    {
        return $this->getData(IBL\Api\Data\SsoPermissionInterface::KEY_ADMIN_ROLE);
    }

    /**
     * @inheritdoc
     */
    public function setAdminRoleId(int $adminRoleId): IBL\Api\Data\SsoPermissionInterface
    {
        return $this->setData(IBL\Api\Data\SsoPermissionInterface::KEY_ADMIN_ROLE, $adminRoleId);
    }

    /**
     * @inheritdoc
     */
    public function getAdminRole(): Authorization\Model\Role
    {
        return $this->adminRole;
    }

    /**
     * @inheritdoc
     */
    public function setAdminRole(Authorization\Model\Role $role): IBL\Api\Data\SsoPermissionInterface
    {
        $this->adminRole = $role;

        return $this->setAdminRoleId($role->getId());
    }

    public function getWhitelistDomainsJson(): string
    {
        return $this->getData(self::KEY_WHITELIST_DOMAINS);
    }

    /**
     * @inheritdoc
     */
    public function getWhitelistDomains(): array
    {
        return $this->whitelistDomains;
    }

    /**
     * @inheritdoc
     */
    public function setWhitelistDomains(
        IBL\Api\Data\VO\WhitelistDomainObjectInterface ...$whitelistDomains
    ): IBL\Api\Data\SsoPermissionInterface {
        $this->whitelistDomains = $whitelistDomains;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function addWhitelistDomain(
        IBL\Api\Data\VO\WhitelistDomainObjectInterface ...$whitelistDomain
    ): IBL\Api\Data\SsoPermissionInterface {
        $this->whitelistDomains = array_merge($this->getWhitelistDomains(), $whitelistDomain);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getCreatedAt(): \DateTimeInterface
    {
        return new \DateTime($this->getData(IBL\Api\Data\SsoPermissionInterface::KEY_CREATED_AT));
    }

    /**
     * @inheritdoc
     */
    public function setCreatedAt(\DateTimeInterface $createdAt): IBL\Api\Data\SsoPermissionInterface
    {
        return $this->setData(
            IBL\Api\Data\SsoPermissionInterface::KEY_CREATED_AT,
            $createdAt->format(IBL\Api\Data\SsoPermissionInterface::DB_DATETIME_FORMAT)
        );
    }

    /**
     * @inheritdoc
     */
    public function getUpdatedAt(): \DateTimeInterface
    {
        return new \DateTime($this->getData(IBL\Api\Data\SsoPermissionInterface::KEY_UPDATED_AT));
    }

    /**
     * @inheritdoc
     */
    public function setUpdatedAt(\DateTimeInterface $updatedAt): IBL\Api\Data\SsoPermissionInterface
    {
        return $this->setData(
            IBL\Api\Data\SsoPermissionInterface::KEY_UPDATED_AT,
            $updatedAt->format(IBL\Api\Data\SsoPermissionInterface::DB_DATETIME_FORMAT)
        );
    }

    /**
     * @inheritdoc
     */
    public function getActiveFrom(): ?\DateTimeInterface
    {
        $activeFrom = $this->getData(IBL\Api\Data\SsoPermissionInterface::KEY_ACTIVE_FROM);

        return $activeFrom === null ? null : new \DateTime($activeFrom);
    }

    /**
     * @inheritdoc
     */
    public function setActiveFrom(?\DateTimeInterface $activeFrom): IBL\Api\Data\SsoPermissionInterface
    {
        return $this->setData(
            IBL\Api\Data\SsoPermissionInterface::KEY_ACTIVE_FROM,
            $activeFrom === null ? null : $activeFrom->format(IBL\Api\Data\SsoPermissionInterface::DB_DATETIME_FORMAT)
        );
    }

    /**
     * @inheritdoc
     */
    public function getActiveUntil(): ?\DateTimeInterface
    {
        $activeUntil = $this->getData(IBL\Api\Data\SsoPermissionInterface::KEY_ACTIVE_UNTIL);

        return $activeUntil === null ? null : new \DateTime($activeUntil);
    }

    /**
     * @inheritdoc
     */
    public function setActiveUntil(?\DateTimeInterface $activeUntil): IBL\Api\Data\SsoPermissionInterface
    {
        return $this->setData(
            IBL\Api\Data\SsoPermissionInterface::KEY_ACTIVE_UNTIL,
            $activeUntil === null ? null : $activeUntil->format(IBL\Api\Data\SsoPermissionInterface::DB_DATETIME_FORMAT)
        );
    }

}