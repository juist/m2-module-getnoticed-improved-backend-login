<?php

namespace GetNoticed\ImprovedBackendLogin\Service;

use Magento\Framework;
use Magento\Authorization;
use GetNoticed\ImprovedBackendLogin as IBL;

class SsoPermissionService
    implements IBL\Api\SsoPermissionServiceInterface
{

    // DI

    /**
     * @var IBL\Api\Data\ValidationResultsInterfaceFactory
     */
    protected $validationResultsFactory;

    /**
     * @var IBL\Providers\ProviderListInterface
     */
    protected $providerList;

    /**
     * @var Authorization\Model\RoleFactory
     */
    protected $roleFactory;

    /**
     * @var Authorization\Model\ResourceModel\Role
     */
    protected $roleResource;

    public function __construct(
        IBL\Api\Data\ValidationResultsInterfaceFactory $validationResultsFactory,
        IBL\Providers\ProviderListInterface $providerList,
        Authorization\Model\RoleFactory $roleFactory,
        Authorization\Model\ResourceModel\Role $roleResource
    ) {
        $this->validationResultsFactory = $validationResultsFactory;
        $this->providerList = $providerList;
        $this->roleFactory = $roleFactory;
        $this->roleResource = $roleResource;
    }

    /**
     * @return IBL\Api\Data\ValidationResultsInterface
     */
    public function validate(IBL\Api\Data\SsoPermissionInterface $permission): IBL\Api\Data\ValidationResultsInterface
    {
        /** @var IBL\Api\Data\ValidationResultsInterface $validationResults */
        $validationResults = $this->validationResultsFactory->create();

        try {
            // Validate required fields
            if ($permission->getProviderCode() === null || strlen($permission->getProviderCode()) < 1) {
                throw new \Exception('No provider specified.');
            }

            try {
                $provider = $this->providerList->getProviderByCode($permission->getProviderCode());
            } catch (Framework\Exception\NoSuchEntityException $e) {
                throw new \Exception('Invalid provider: no such provider exists.');
            }

            if ($provider->isActive() !== true) {
                throw new \Exception(
                    'Chosen provider is not active - please configure it through Stores > Configuration first.'
                );
            }

            try {
                $adminRole = $this->getAdminRoleById($permission->getAdminRoleId());
            } catch (Framework\Exception\NoSuchEntityException $e) {
                throw new \Exception('Admin user does not exist.');
            }

            if ($adminRole->getRoleType() !== Authorization\Model\Acl\Role\Group::ROLE_TYPE) {
                throw new \Exception('Only group roles can be chosen for SSO permissions.');
            }

            if (count($permission->getWhitelistDomains()) < 1) {
                throw new \Exception('Must select at least 1 whitelist domain: can not allow all domains.');
            }

            return $validationResults->setIsValid(true)->setMessages([]);
        } catch (\Exception $e) {
            return $validationResults->setIsValid(false)->setMessages([$e->getMessage()]);
        }
    }

    /**
     * @param int $roleId
     *
     * @return \Magento\Authorization\Model\Role
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function getAdminRoleById(int $roleId): Authorization\Model\Role
    {
        $role = $this->roleFactory->create();
        $this->roleResource->load($role, $roleId);

        if ($role->getId() === null) {
            throw Framework\Exception\NoSuchEntityException::singleField('entity_id', $roleId);
        }

        return $role;
    }

}