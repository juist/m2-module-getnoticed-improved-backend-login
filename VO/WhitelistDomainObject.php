<?php

namespace GetNoticed\ImprovedBackendLogin\VO;

use GetNoticed\ImprovedBackendLogin as IBL;

class WhitelistDomainObject
    implements IBL\Api\Data\VO\WhitelistDomainObjectInterface
{

    /**
     * @var string
     */
    protected $hostname;

    /**
     * @var string
     */
    protected $tld;

    /**
     * @var string
     */
    protected $user;

    public function toArray(): array
    {
        return [
            'user'     => $this->getUser(),
            'hostname' => $this->getHostname(),
            'tld'      => $this->getTld()
        ];
    }

    public function getHostname(): string
    {
        return $this->hostname;
    }

    public function setHostname(string $hostname): IBL\Api\Data\VO\WhitelistDomainObjectInterface
    {
        $this->hostname = $hostname;

        return $this;
    }

    public function getTld(): string
    {
        return $this->tld;
    }

    public function setTld(string $tld): IBL\Api\Data\VO\WhitelistDomainObjectInterface
    {
        $this->tld = $tld;

        return $this;
    }

    public function getUser(): string
    {
        return $this->user;
    }

    public function setUser(string $user): IBL\Api\Data\VO\WhitelistDomainObjectInterface
    {
        $this->user = $user;

        return $this;
    }

}