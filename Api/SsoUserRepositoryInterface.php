<?php

namespace GetNoticed\ImprovedBackendLogin\Api;

use Magento\User;
use GetNoticed\ImprovedBackendLogin as IBL;

interface SsoUserRepositoryInterface
{

    /**
     * @param \Magento\User\Api\Data\UserInterface|\Magento\User\Model\User $adminUser
     *
     * @return IBL\Api\Data\SsoUserInterface|IBL\Model\SsoUser
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getByAdminUser(User\Api\Data\UserInterface $adminUser): IBL\Api\Data\SsoUserInterface;

    /**
     * @param IBL\Api\Data\SsoUserInterface|IBL\Model\SsoUser $ssoUser
     *
     * @return IBL\Api\Data\SsoUserInterface|IBL\Model\SsoUser
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     * @throws \Exception
     */
    public function save(IBL\Api\Data\SsoUserInterface $ssoUser): IBL\Api\Data\SsoUserInterface;

    /**
     * @param IBL\Api\Data\SsoUserInterface|IBL\Model\SsoUser $ssoUser
     *
     * @return bool
     */
    public function delete(IBL\Api\Data\SsoUserInterface $ssoUser): bool;

}