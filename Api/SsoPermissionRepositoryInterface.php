<?php

namespace GetNoticed\ImprovedBackendLogin\Api;

use Magento\User;
use GetNoticed\ImprovedBackendLogin as IBL;

interface SsoPermissionRepositoryInterface
{

    /**
     * @return IBL\Api\Data\SsoPermissionInterface|IBL\Model\SsoPermission
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get(int $entityId): IBL\Api\Data\SsoPermissionInterface;

    /**
     * @param IBL\Api\Data\SsoPermissionInterface|IBL\Model\SsoPermission $ssoPermission
     *
     * @return IBL\Api\Data\SsoPermissionInterface|IBL\Model\SsoPermission
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     * @throws \Exception
     */
    public function save(IBL\Api\Data\SsoPermissionInterface $ssoPermission): IBL\Api\Data\SsoPermissionInterface;

    /**
     * @param IBL\Api\Data\SsoPermissionInterface|IBL\Model\SsoPermission $ssoPermission
     */
    public function delete(IBL\Api\Data\SsoPermissionInterface $ssoPermission): bool;

    /**
     * @throws IBL\Exception\NoSuchSsoPermissionException
     */
    public function match(
        IBL\Providers\ProviderInterface $provider,
        User\Api\Data\UserInterface $user
    ): IBL\Api\Data\SsoPermissionInterface;

}