<?php

namespace GetNoticed\ImprovedBackendLogin\Api;

use GetNoticed\ImprovedBackendLogin as IBL;

interface SsoPermissionServiceInterface
{

    /**
     * @return IBL\Api\Data\ValidationResultsInterface
     */
    public function validate(IBL\Api\Data\SsoPermissionInterface $permission): IBL\Api\Data\ValidationResultsInterface;

}