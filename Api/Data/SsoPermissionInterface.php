<?php

namespace GetNoticed\ImprovedBackendLogin\Api\Data;

use Magento\Authorization;
use GetNoticed\ImprovedBackendLogin as IBL;

interface SsoPermissionInterface
{

    const TABLE_NAME = 'getnoticed_ibl_sso_permission';
    const PRIMARY_KEY_FIELD = 'entity_id';

    const KEY_PROVIDER_CODE = 'provider_code';
    const KEY_ADMIN_ROLE = 'admin_role_id';
    const KEY_WHITELIST_DOMAINS = 'whitelist_domains';
    const KEY_CREATED_AT = 'created_at';
    const KEY_UPDATED_AT = 'updated_at';
    const KEY_ACTIVE_FROM = 'active_from';
    const KEY_ACTIVE_UNTIL = 'active_until';

    const DB_DATETIME_FORMAT = 'Y-m-d H:i:s';

    public function getProviderCode(): string;

    /**
     * @param string $providerCode
     *
     * @return IBL\Api\Data\SsoPermissionInterface|IBL\Model\SsoPermission
     */
    public function setProviderCode(string $providerCode): IBL\Api\Data\SsoPermissionInterface;

    public function getProvider(): IBL\Providers\ProviderInterface;

    /**
     * @return IBL\Api\Data\SsoPermissionInterface|IBL\Model\SsoPermission
     */
    public function setProvider(IBL\Providers\ProviderInterface $provider): IBL\Api\Data\SsoPermissionInterface;

    public function getAdminRoleId(): int;

    /**
     * @return IBL\Api\Data\SsoPermissionInterface|IBL\Model\SsoPermission
     */
    public function setAdminRoleId(int $adminRoleId): IBL\Api\Data\SsoPermissionInterface;

    public function getAdminRole(): Authorization\Model\Role;

    /**
     * @return IBL\Api\Data\SsoPermissionInterface|IBL\Model\SsoPermission
     */
    public function setAdminRole(Authorization\Model\Role $role): IBL\Api\Data\SsoPermissionInterface;

    public function getWhitelistDomainsJson(): string;

    /**
     * @return IBL\Api\Data\VO\WhitelistDomainObjectInterface[]|IBL\VO\WhitelistDomainObject[]
     */
    public function getWhitelistDomains(): array;

    /**
     * @param IBL\Api\Data\VO\WhitelistDomainObjectInterface[]|IBL\VO\WhitelistDomainObject[] ...$whitelistDomains
     *
     * @return IBL\Api\Data\SsoPermissionInterface|IBL\Model\SsoPermission
     */
    public function setWhitelistDomains(
        IBL\Api\Data\VO\WhitelistDomainObjectInterface ...$whitelistDomains
    ): IBL\Api\Data\SsoPermissionInterface;

    /**
     * @param IBL\Api\Data\VO\WhitelistDomainObjectInterface[]|IBL\VO\WhitelistDomainObject[] $whitelistDomain
     *
     * @return IBL\Api\Data\SsoPermissionInterface|IBL\Model\SsoPermission
     */
    public function addWhitelistDomain(
        IBL\Api\Data\VO\WhitelistDomainObjectInterface ...$whitelistDomain
    ): IBL\Api\Data\SsoPermissionInterface;

    /**
     * @return \DateTimeInterface|\DateTime
     */
    public function getCreatedAt(): \DateTimeInterface;

    /**
     * @param \DateTimeInterface|\DateTime $createdAt
     *
     * @return IBL\Api\Data\SsoPermissionInterface|IBL\Model\SsoPermission
     */
    public function setCreatedAt(\DateTimeInterface $createdAt): IBL\Api\Data\SsoPermissionInterface;

    /**
     * @return \DateTimeInterface|\DateTime
     */
    public function getUpdatedAt(): \DateTimeInterface;

    /**
     * @param \DateTimeInterface|\DateTime $updatedAt
     *
     * @return IBL\Api\Data\SsoPermissionInterface|IBL\Model\SsoPermission
     */
    public function setUpdatedAt(\DateTimeInterface $updatedAt): IBL\Api\Data\SsoPermissionInterface;

    /**
     * @return \DateTimeInterface|\DateTime|null
     */
    public function getActiveFrom(): ?\DateTimeInterface;

    /**
     * @param \DateTimeInterface|\DateTime|null $activeFrom
     *
     * @return IBL\Api\Data\SsoPermissionInterface|IBL\Model\SsoPermission
     */
    public function setActiveFrom(?\DateTimeInterface $activeFrom): IBL\Api\Data\SsoPermissionInterface;

    /**
     * @return \DateTimeInterface|\DateTime|null
     */
    public function getActiveUntil(): ?\DateTimeInterface;

    /**
     * @param \DateTimeInterface|\DateTime|null $activeUntil
     *
     * @return IBL\Api\Data\SsoPermissionInterface|IBL\Model\SsoPermission
     */
    public function setActiveUntil(?\DateTimeInterface $activeUntil): IBL\Api\Data\SsoPermissionInterface;

}