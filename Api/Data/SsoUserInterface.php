<?php

namespace GetNoticed\ImprovedBackendLogin\Api\Data;

use Magento\User;
use GetNoticed\ImprovedBackendLogin as IBL;

interface SsoUserInterface
{

    const TABLE_NAME = 'getnoticed_ibl_sso_user';
    const PRIMARY_KEY_FIELD = 'sso_user_id';

    const KEY_USER_ID = 'user_id';
    const KEY_FIRST_LOGIN_AT = 'first_login_at';
    const KEY_LAST_LOGIN_AT = 'last_login_at';
    const KEY_LOGIN_COUNT = 'login_count';

    /**
     * @return \Magento\User\Api\Data\UserInterface|\Magento\User\Model\User
     */
    public function getUser(): User\Api\Data\UserInterface;

    public function getUserId(): int;

    /**
     * @param \Magento\User\Api\Data\UserInterface|\Magento\User\Model\User $user
     */
    public function setUser(User\Api\Data\UserInterface $user): IBL\Api\Data\SsoUserInterface;

    /**
     * @return \DateTimeInterface|\DateTime
     */
    public function getFirstLoginAt(): \DateTimeInterface;

    /**
     * @param \DateTimeInterface|\DateTime $firstLoginAt
     */
    public function setFirstLoginAt(\DateTimeInterface $firstLoginAt): IBL\Api\Data\SsoUserInterface;

    /**
     * @return \DateTimeInterface|\DateTime
     */
    public function getLastLoginAt(): \DateTimeInterface;

    /**
     * @param \DateTimeInterface|\DateTime $lastLoginAt
     */
    public function setLastLoginAt(\DateTimeInterface $lastLoginAt): IBL\Api\Data\SsoUserInterface;

    public function getLoginCount(): int;

    public function setLoginCount(int $loginCount): IBL\Api\Data\SsoUserInterface;

    public function addLoginCount(): IBL\Api\Data\SsoUserInterface;

}