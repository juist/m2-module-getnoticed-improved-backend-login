<?php

namespace GetNoticed\ImprovedBackendLogin\Api\Data\VO;

use GetNoticed\ImprovedBackendLogin as IBL;

interface WhitelistDomainObjectInterface
{

    public function toArray(): array;

    public function getHostname(): string;

    public function setHostname(string $hostname): IBL\Api\Data\VO\WhitelistDomainObjectInterface;

    public function getTld(): string;

    public function setTld(string $tld): IBL\Api\Data\VO\WhitelistDomainObjectInterface;

    public function getUser(): string;

    public function setUser(string $user): IBL\Api\Data\VO\WhitelistDomainObjectInterface;

}