<?php

namespace GetNoticed\ImprovedBackendLogin\Setup;

use Magento\Framework;
use Magento\Framework\DB\Ddl\Table as DT;
use GetNoticed\ImprovedBackendLogin as IBL;

class InstallSchema
    implements Framework\Setup\InstallSchemaInterface
{

    /**
     * @inheritdoc
     */
    public function install(
        Framework\Setup\SchemaSetupInterface $setup,
        Framework\Setup\ModuleContextInterface $context
    ) {
        $setup->startSetup();
        $this->addSsoUserTable($setup, $setup->getConnection());
        $this->addSsoPermissionTable($setup, $setup->getConnection());
        $setup->endSetup();
    }

    /**
     * @throws \Zend_Db_Exception
     */
    private function addSsoUserTable(
        Framework\Setup\SetupInterface $setup,
        Framework\DB\Adapter\AdapterInterface $adapter
    ): void {
        $adapter->createTable(
            $adapter->newTable($setup->getTable(IBL\Api\Data\SsoUserInterface::TABLE_NAME))
                    ->setComment('GetNoticed IBL - User Table')
                    ->addColumn(
                        IBL\Api\Data\SsoUserInterface::PRIMARY_KEY_FIELD,
                        DT::TYPE_INTEGER,
                        null,
                        [
                            'primary'  => true,
                            'unsigned' => true,
                            'identity' => true,
                            'nullable' => false
                        ],
                        'SSO User ID'
                    )
                    ->addColumn(
                        IBL\Api\Data\SsoUserInterface::KEY_USER_ID,
                        DT::TYPE_INTEGER,
                        null,
                        [
                            'unsigned' => true,
                            'nullable' => false
                        ],
                        'User ID'
                    )
                    ->addForeignKey(
                        $adapter->getForeignKeyName(
                            IBL\Api\Data\SsoUserInterface::TABLE_NAME,
                            IBL\Api\Data\SsoUserInterface::KEY_USER_ID,
                            'admin_user',
                            'user_id'
                        ),
                        IBL\Api\Data\SsoUserInterface::KEY_USER_ID,
                        $setup->getTable('admin_user'),
                        'user_id',
                        DT::ACTION_CASCADE
                    )
                    ->addColumn(
                        IBL\Api\Data\SsoUserInterface::KEY_FIRST_LOGIN_AT,
                        DT::TYPE_DATETIME,
                        null,
                        [
                            'nullable' => false
                        ],
                        'First login'
                    )
                    ->addColumn(
                        IBL\Api\Data\SsoUserInterface::KEY_LAST_LOGIN_AT,
                        DT::TYPE_DATETIME,
                        null,
                        [
                            'nullable' => false
                        ],
                        'Last login'
                    )
                    ->addColumn(
                        IBL\Api\Data\SsoUserInterface::KEY_LOGIN_COUNT,
                        DT::TYPE_INTEGER,
                        null,
                        [
                            'nullable' => false,
                            'default'  => 0
                        ],
                        'Login count'
                    )
        );
    }

    /**
     * @throws \Zend_Db_Exception
     */
    private function addSsoPermissionTable(
        Framework\Setup\SetupInterface $setup,
        Framework\DB\Adapter\AdapterInterface $adapter
    ): void {
        $adapter->createTable(
            $adapter->newTable($setup->getTable(IBL\Api\Data\SsoPermissionInterface::TABLE_NAME))
                    ->setComment('GetNoticed IBL - Permission Table')
                    ->addColumn(
                        IBL\Api\Data\SsoPermissionInterface::PRIMARY_KEY_FIELD,
                        DT::TYPE_INTEGER,
                        null,
                        [
                            'primary'  => true,
                            'unsigned' => true,
                            'identity' => true,
                            'nullable' => false
                        ],
                        'Entity ID'
                    )
                    ->addColumn(
                        IBL\Api\Data\SsoPermissionInterface::KEY_PROVIDER_CODE,
                        DT::TYPE_TEXT,
                        255,
                        [
                            'nullable' => false
                        ],
                        'SSO Provider Code'
                    )
                    ->addColumn(
                        IBL\Api\Data\SsoPermissionInterface::KEY_ADMIN_ROLE,
                        DT::TYPE_INTEGER,
                        null,
                        [
                            'unsigned' => true,
                            'nullable' => false
                        ],
                        'Admin Role ID'
                    )
                    ->addForeignKey(
                        $adapter->getForeignKeyName(
                            IBL\Api\Data\SsoPermissionInterface::TABLE_NAME,
                            IBL\Api\Data\SsoPermissionInterface::KEY_ADMIN_ROLE,
                            'authorization_role',
                            'role_id'
                        ),
                        IBL\Api\Data\SsoPermissionInterface::KEY_ADMIN_ROLE,
                        $setup->getTable('authorization_role'),
                        'role_id',
                        DT::ACTION_CASCADE
                    )
                    ->addColumn(
                        IBL\Api\Data\SsoPermissionInterface::KEY_WHITELIST_DOMAINS,
                        DT::TYPE_TEXT,
                        null,
                        [
                            'nullable' => false
                        ],
                        'Whitelist domains'
                    )
                    ->addColumn(
                        IBL\Api\Data\SsoPermissionInterface::KEY_CREATED_AT,
                        DT::TYPE_DATETIME,
                        null,
                        [
                            'nullable' => false
                        ],
                        'Created At'
                    )
                    ->addColumn(
                        IBL\Api\Data\SsoPermissionInterface::KEY_UPDATED_AT,
                        DT::TYPE_DATETIME,
                        null,
                        [
                            'nullable' => false
                        ],
                        'Updated At'
                    )
                    ->addColumn(
                        IBL\Api\Data\SsoPermissionInterface::KEY_ACTIVE_FROM,
                        DT::TYPE_DATETIME,
                        null,
                        [
                            'nullable' => true
                        ],
                        'Active From'
                    )
                    ->addColumn(
                        IBL\Api\Data\SsoPermissionInterface::KEY_ACTIVE_UNTIL,
                        DT::TYPE_DATETIME,
                        null,
                        [
                            'nullable' => true
                        ],
                        'Active Until'
                    )
        );
    }

}