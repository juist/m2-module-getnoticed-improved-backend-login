# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
* Setup basic module skeleton
* Add new provider: Google
* Put a system into place to gather all (active) providers from 1 central location
* Admin login now uses active provider list to display SSO buttons
* All admin logins from SSO users are now logged - once logged in with SSO, regular login is disabled
* Create an advanced settings admin grid/form where admins can define who may login with SSO (SsoPermission)
* Deny logging in if no SsoPermission is present for the domain and/or user