<?php

namespace GetNoticed\ImprovedBackendLogin\Plugin;

use Magento\Framework;
use Magento\User;
use Magento\Authorization;
use GetNoticed\ImprovedBackendLogin as IBL;

class OverrideMatchMethodForDevelopProvider
{

    // DI

    /**
     * @var Framework\App\State
     */
    protected $appState;

    /**
     * @var IBL\Model\SsoPermissionFactory
     */
    protected $ssoPermissionFactory;

    /**
     * @var Authorization\Model\ResourceModel\Role\CollectionFactory
     */
    protected $roleCollectionFactory;

    /**
     * @var IBL\Api\Data\VO\WhitelistDomainObjectInterfaceFactory
     */
    protected $whitelistDomainObjectFactory;

    /**
     * @var IBL\Helper\Provider\DevelopProviderHelper
     */
    protected $developProviderHelper;

    public function __construct(
        Framework\App\State $appState,
        IBL\Model\SsoPermissionFactory $ssoPermissionFactory,
        Authorization\Model\ResourceModel\Role\CollectionFactory $roleCollectionFactory,
        IBL\Api\Data\VO\WhitelistDomainObjectInterfaceFactory $whitelistDomainObjectFactory,
        IBL\Helper\Provider\DevelopProviderHelper $developProviderHelper
    ) {
        $this->appState = $appState;
        $this->ssoPermissionFactory = $ssoPermissionFactory;
        $this->roleCollectionFactory = $roleCollectionFactory;
        $this->whitelistDomainObjectFactory = $whitelistDomainObjectFactory;
        $this->developProviderHelper = $developProviderHelper;
    }

    public function aroundMatch(
        IBL\Api\SsoPermissionRepositoryInterface $subject,
        callable $next,
        IBL\Providers\ProviderInterface $provider,
        User\Api\Data\UserInterface $user
    ): IBL\Api\Data\SsoPermissionInterface {
        if ($this->appState->getMode() !== Framework\App\State::MODE_DEVELOPER) {
            return $next($provider, $user);
        }

        /** @var IBL\Api\Data\SsoPermissionInterface|IBL\Model\SsoPermission $ssoPermission */
        $ssoPermission = $this->ssoPermissionFactory->create();
        $now = new \Datetime();
        $ssoPermission
            ->setProviderCode(IBL\Providers\Provider\DevelopProvider::CODE)
            ->setAdminRole($this->getDeveloperAdminRole())
            ->setWhitelistDomains($this->getWhitelistDomains())
            ->setCreatedAt($now)
            ->setUpdatedAt($now);

        return $ssoPermission;
    }

    /**
     * @return \Magento\Framework\DataObject|Authorization\Model\Role
     */
    private function getDeveloperAdminRole()
    {
        /** @var Authorization\Model\ResourceModel\Role\Collection $collection */
        $collection = $this->roleCollectionFactory->create();
        $collection->addFieldToFilter('role_name', ['eq' => 'Administrators']);

        return $collection->getFirstItem();
    }

    private function getWhitelistDomains(): IBL\Api\Data\VO\WhitelistDomainObjectInterface
    {
        $developerWhitelist = $this->whitelistDomainObjectFactory->create();
        $developerWhitelist
            ->setUser($this->developProviderHelper->getEmailUser())
            ->setHostname($this->developProviderHelper->getEmailHostname())
            ->setTld($this->developProviderHelper->getEmailTld());

        return $developerWhitelist;
    }

}