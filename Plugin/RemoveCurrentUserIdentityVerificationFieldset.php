<?php

namespace GetNoticed\ImprovedBackendLogin\Plugin;

use Magento\Framework;
use Magento\Backend;
use Magento\User;
use GetNoticed\ImprovedBackendLogin as IBL;

class RemoveCurrentUserIdentityVerificationFieldset
{

    const CURRENT_USER_IDENTITY_VERIFICATION_FIELDSET_KEY = 'current_user_verification_fieldset';

    /**
     * @var Backend\Model\Auth\Session
     */
    protected $backendSession;

    /**
     * @var IBL\Api\SsoUserRepositoryInterface
     */
    protected $ssoUserRepository;

    /**
     * @inheritDoc
     */
    public function __construct(
        Backend\Model\Auth\Session $backendSession,
        IBL\Api\SsoUserRepositoryInterface $ssoUserRepository
    ) {
        $this->backendSession = $backendSession;
        $this->ssoUserRepository = $ssoUserRepository;
    }

    public function beforeSetForm(
        Backend\Block\Widget\Form $subject,
        Framework\Data\Form $form
    ) {
        if ($this->shouldRemoveCurrentUserVerificationFieldset()) {
            $fieldset = $form->getElement(self::CURRENT_USER_IDENTITY_VERIFICATION_FIELDSET_KEY);

            if ($fieldset instanceof Framework\Data\Form\Element\Fieldset) {
                $fieldset->removeField(User\Block\User\Edit\Tab\Main::CURRENT_USER_PASSWORD_FIELD);
                $fieldset->removeField(sprintf(
                                           '%s_notice',
                                           User\Block\User\Edit\Tab\Main::CURRENT_USER_PASSWORD_FIELD
                                       ));
                $fieldset->addField(
                    User\Block\User\Edit\Tab\Main::CURRENT_USER_PASSWORD_FIELD,
                    'hidden',
                    [
                        'name'  => User\Block\User\Edit\Tab\Main::CURRENT_USER_PASSWORD_FIELD,
                        'value' => 'empty'
                    ]
                );
                $fieldset->addField(
                    sprintf('%s_notice', User\Block\User\Edit\Tab\Main::CURRENT_USER_PASSWORD_FIELD),
                    'note',
                    [
                        'label' => __('Verification skipped'),
                        'text'  => __('Current User Identity Verification will be skipped for SSO users.')
                    ]
                );
                $fieldset->addElementValues([User\Block\User\Edit\Tab\Main::CURRENT_USER_PASSWORD_FIELD => 'test']);
            }
        }

        return [$form];
    }

    /**
     * @return bool
     */
    protected function shouldRemoveCurrentUserVerificationFieldset(): bool
    {
        try {
            $adminUser = $this->backendSession->getUser();
            $ssoUser = $this->ssoUserRepository->getByAdminUser($adminUser);

            return true;
        } catch (Framework\Exception\NoSuchEntityException | Framework\Exception\NoSuchEntityException $e) {
            return false;
        }
    }

}
