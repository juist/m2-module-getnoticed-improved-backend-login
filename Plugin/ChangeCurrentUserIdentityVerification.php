<?php

namespace GetNoticed\ImprovedBackendLogin\Plugin;

use Magento\Framework;
use Magento\User;
use GetNoticed\ImprovedBackendLogin as IBL;

class ChangeCurrentUserIdentityVerification
{

    /**
     * @var IBL\Api\SsoUserRepositoryInterface
     */
    protected $ssoUserRepository;

    public function __construct(
        IBL\Api\SsoUserRepositoryInterface $ssoUserRepository
    ) {
        $this->ssoUserRepository = $ssoUserRepository;
    }

    public function aroundPerformIdentityCheck(
        User\Model\User $subject,
        callable $proceed,
        $passwordString
    ) {
        try {
            $ssoUser = $this->ssoUserRepository->getByAdminUser($subject);

            // Do not check auth for SSO users
            return $this;
        } catch (Framework\Exception\NoSuchEntityException $e) {
            return $proceed($passwordString);
        }
    }

}